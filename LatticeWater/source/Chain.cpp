#include "Chain.hpp"
#include "Lattice.hpp"
#include "Stats.hpp"
#include "StatsMethods.hpp"
#include "EnergyMap.hpp"
#include "WaterStats.hpp"

#include <math.h>
#include <stdio.h>
#include <iostream>
#include <cstdlib>

//#define DEBUG

using namespace std;

/******************Standard Move **************************/
/* Generate new configuration                        */
/* Check if newConfig illegal,if illegal return -1   */
/* Calculate energy difference between states        */
/* Sample boths states for statistics                */
/* accept or reject move                             */
/* reject: return 0                                  */
/* accept: update lattice and chain,return 1         */
/**********************************************************/



Chain::Chain(Lattice *l1,int chnum){
  l=l1;
  chainNum=chnum;
  frozen =false;
  locked=false;
  //Cext=0;
  // betaMoves= 0.05;
}


Chain::~Chain(){
  for(int i=0;i<N;i++){
    delete residues[i];
  }

}

void Chain::setChainNum(int n){
  chainNum=n;
}


/*void Chain::setRandomSpins(){
  setBkwdFwdSpins();
  Pos dir;
  for(int n=0;n<N;n++){
    do{
      dir = local[(int) floor(6 * drand48())];
    }while(dir != residues[n]->bkwd && dir != residues[n]->fwd );
    residues[n]->spin= dir;
  }  
  }*/

/*
void Chain::setBkwdFwdSpins(){
  residues[0]->bkwd=Pos(0,0,0);
  residues[0]->fwd=residues[1]->pos -residues[0]->pos;
  residues[N-1]->fwd=Pos(0,0,0);
  residues[N-1]->bkwd=residues[N-2]->pos -residues[N-1]->pos;
  for(int n=1;n<N-1;n++){
    residues[n]->bkwd=residues[n-1]->pos -residues[n]->pos;
    residues[n]->fwd =residues[n+1]->pos -residues[n]->pos;
  }
  }*/


/*void Chain::setStateResidues(){
  for(int n=0;n<N;n++){
    residues[n]->state=inCoil;
  }
  }*/

/*int Chain::shuffleSpin(){
  int n =(int) floor(N * drand48());
  if(residues[n]->state==inStrand)return -1;
  Pos dir = local[(int) floor(6 * drand48())];
  if(dir == residues[n]->bkwd || dir == residues[n]->fwd){
    return -1;
  }  				  
  //TODO add energy acceptance + sampling
  
  residues[n]->spin = dir;
  return 0;
  }*/

int Chain::localMove(){
  Pos posNew;
  int px=0,py=0,pz=0; 
  int n = (int) floor(N * drand48());
#ifdef DEBUG
  cout <<"local "<<n<<endl;
#endif
  if(n<0 ||n >= N){ cout<<"residue not in range"<<n<<endl; exit(-1);} 
  if(n==0 || n == N-1){ // end of chain choose one of 6 directions directions
    int nn =1;
    if(n==(N-1)){nn = N-2;} // nearest neighbour   
    int rr= (int)floor(6 * drand48()); 
    // OPT slow, Behnaz: put links instead
    posNew = local[rr] + residues[nn]->pos;
    posNew.periodicBoundary();
  }else{ // try cornerflip
    px = (residues[n]->pos.x == residues[n-1]->pos.x) & (residues[n]->pos.x == residues[n+1]->pos.x);
    py = (residues[n]->pos.y == residues[n-1]->pos.y) & (residues[n]->pos.y == residues[n+1]->pos.y);
    pz = (residues[n]->pos.z == residues[n-1]->pos.z) & (residues[n]->pos.z == residues[n+1]->pos.z);
    if(px+py+pz==2){
      // on a straight line
      l->sampleCurrentStats();
      return -1;
    }else if(px+py+pz==1){ 
      if(px){
	posNew.x=residues[n]->pos.x;
	posNew.y =  residues[n-1]->pos.y + residues[n+1]->pos.y - residues[n]->pos.y;
	posNew.z =  residues[n-1]->pos.z + residues[n+1]->pos.z - residues[n]->pos.z;
      }else if(py){
	posNew.y=residues[n]->pos.y;
	posNew.x =  residues[n-1]->pos.x + residues[n+1]->pos.x - residues[n]->pos.x;
 	posNew.z =  residues[n-1]->pos.z + residues[n+1]->pos.z - residues[n]->pos.z;
      }else{
	posNew.z=residues[n]->pos.z;
	posNew.x =  residues[n-1]->pos.x + residues[n+1]->pos.x - residues[n]->pos.x;
	posNew.y =  residues[n-1]->pos.y + residues[n+1]->pos.y - residues[n]->pos.y;
      }
    }else{
      cout << "break in chain: "<< chainNum <<endl;;
      cout << "trying to move:"<<n<< " with coords:" ;
      cout <<residues[n]->pos.x<<" "<<residues[n]->pos.y<<" "<<residues[n]->pos.z<<" "<<endl;;
      cout <<residues[n-1]->pos.x<<" "<<residues[n-1]->pos.y<<" "<<residues[n-1]->pos.z<<" "<<endl;
      cout <<residues[n+1]->pos.x<<" "<<residues[n+1]->pos.y<<" "<<residues[n+1]->pos.z<<" "<<endl;
      //printChain();
      l->writePDBMultiChain("chainBreak.pdb");
      //return -1;
       exit(1);
    }


  }
  if(l->getResidue(posNew)!=NULL){
    if(l->getResidue(posNew)->n == n-2 && l->getResidue(posNew)->chainNum == chainNum){
      return crankshaft(n,n-1,px,py,pz);
    }else if(l->getResidue(posNew)->n == n+2 && l->getResidue(posNew)->chainNum== chainNum){
      return crankshaft(n,n+1,px,py,pz);
    }else{
      l->sampleCurrentStats();
      return(-1);
    }
  }
  
  Residue * res=residues[n];

  //// update lattice
  l->emptyLatticePos(res->pos);

  oldLocalStats.clean();
  newLocalStats.clean();


  //// old stats
  oldLocalStats.localStats(residues[n],residues[n]->pos,l);
  oldLocalStats.solventStats(posNew,l);

  //// new stats
  newLocalStats.localStats(residues[n],posNew,l);
  newLocalStats.solventStats(res->pos,l);

  //// lattice stats
  newLatticeStats.clean();
  newLatticeStats =  l->stats.delta(newLocalStats,oldLocalStats);


  //// Water Contacts
  //// old config
  Pos posOld = res->pos;
  l->waterStats->localWC(posNew,NULL,true);
  l->waterStats->localWC(posOld,residues[n],true);
  //// new config
  l->waterStats->localWC(posOld,NULL,false);
  l->waterStats->localWC(posNew,residues[n],false);

  Wstats deltaWC = l->waterStats->calculateChangeInWC();

  
  int dE= newLocalStats.getDeltaE(oldLocalStats) + deltaWC.solvE;
  int mdE=0;
  double boltz;
  if(App::umbrellaSampling){
    mdE=dE + App::SpringConstant_Nint*( newLatticeStats.getNint()*newLatticeStats.getNint() - l->stats.getNint()*l->stats.getNint()+2*App::Q0*(l->stats.getNint()-newLatticeStats.getNint()))+
	App::SpringConstant_Cint*(newLatticeStats.getCint()*newLatticeStats.getCint() - l->stats.getCint()*l->stats.getCint()+2*App::C0*(l->stats.getCint()-newLatticeStats.getCint()));
    boltz=exp(((-(float)(mdE)))* l->getBetaMoves());
  }
  else{
    boltz = exp(((-(float) (dE)) )* l->getBetaMoves());
  }
  double bb = boltz/(boltz+1.0);
  if(isinf(boltz)){
      bb =1.0;
  }
  if(l->energyMap != NULL){
    l->energyMap->mapStats(l->stats,1.0 - bb, l->waterStats->totalE);
    l->energyMap->mapStats(newLatticeStats,bb,l->waterStats->totalE +dE );
  }
  bool accept=false;
  if(App::umbrellaSampling){
    if(mdE<=0 || drand48() < boltz){
      accept=true;
    }
  }
  else{
    if(dE<=0 || drand48() < boltz){
      accept=true;
    }
  }
  if(accept){
    // update position and lattice 
    residues[n]->pos = posNew;
    l->setResidue(posNew,residues[n]);
    // update lattice stats
    l->stats = newLatticeStats;

    //// WC
    l->waterStats->updateWC(deltaWC,dE);
    l->waterStats->cleanUpWC();
    if(dE==0){
      return(1);
    }else{
      return(2);
    }
  }else{
    l->setResidue(res->pos,res);
    l->waterStats->cleanUpWC();
    return(0);
  }
}



int Chain::crankshaft(int n1, int n2,int px, int py, int pz){
  // cout << "crankshaft "<<n1<<" "<<n2<<endl;
  int n0,n3;
  Residue * resn1 = residues[n1];
  Residue * resn2 = residues[n2];
  

  if(n1<n2){
    n0=n1-1;
    n3=n1+2;
  }else{
    n0=n1+1;
    n3=n1-2;
  }

  // Residue * resn0 = residues[n0];
  //Residue * resn3 = residues[n3];

  if(n0<0 || n3<0 || n0>=N || n3>=N){
    l->sampleCurrentStats();
    return -3;
  }
  // OPT only need dirOrthogonal
  int dirStalk,dirLegs,dirOrthogonal;
  if(px){
    dirOrthogonal = XDIR;
    if((residues[n1]->pos.y - residues[n2]->pos.y)==0){
      dirStalk=YDIR;
      dirLegs=ZDIR;
    }else{
      dirStalk=ZDIR;
      dirLegs=YDIR;
    }
  }else if(py){
    dirOrthogonal = YDIR;
    if((residues[n1]->pos.x - residues[n2]->pos.x)==0){
      dirStalk=XDIR;
      dirLegs=ZDIR;
    }else{
      dirStalk=ZDIR;
      dirLegs=XDIR;
    }
  }else {
    dirOrthogonal = ZDIR;
    if((residues[n1]->pos.x - residues[n2]->pos.x)==0){
      dirStalk=XDIR;
      dirLegs=YDIR;
    }else{
      dirStalk=XDIR;
      dirLegs=YDIR;
    }
  }
  int dirFlip;
  if( drand48()<0.5){
    dirFlip=1;
  }else{
    dirFlip=-1;
  }
 
  Pos old0 =Pos(residues[n0]->pos.x,residues[n0]->pos.y,residues[n0]->pos.z);
  Pos old3 =Pos(residues[n3]->pos.x,residues[n3]->pos.y,residues[n3]->pos.z);

  Pos newPos1;
  Pos newPos2;

  newPos1[dirOrthogonal]= old0[dirOrthogonal] + dirFlip;
  newPos2[dirOrthogonal]= old3[dirOrthogonal] + dirFlip;

  newPos1[dirStalk]= old0[dirStalk];
  newPos2[dirStalk]= old3[dirStalk];
 
  newPos1[dirLegs]= old0[dirLegs];
  newPos2[dirLegs]= old3[dirLegs];


  newPos1.periodicBoundary();
  newPos2.periodicBoundary();



  // test for collision
  if(l->getResidue(newPos1)!=NULL || 
     l->getResidue(newPos2)!=NULL){
    l->sampleCurrentStats();
    return -1;
  }
  


  //// remove residues involved in move from lattice
  l->emptyLatticePos(resn1->pos);
  l->emptyLatticePos(resn2->pos);


  //// old Stats
  oldLocalStats.clean();
  
  oldLocalStats.localStats(resn1,resn1->pos ,l);
  oldLocalStats.localStats(resn2,resn2->pos ,l);

  oldLocalStats.solventStats(newPos1,l);
  oldLocalStats.solventStats(newPos2,l);

  //// calculate new stats
  newLocalStats.clean();
  
  newLocalStats.localStats(resn1,newPos1,l);
  newLocalStats.localStats(resn2,newPos2,l);
  
  newLocalStats.solventStats(resn1->pos,l);
  newLocalStats.solventStats(resn2->pos,l);

  //// lattice stats
  newLatticeStats.clean();
  newLatticeStats =  l->stats.delta(newLocalStats,oldLocalStats);

  //// Water Contacts
  
  //// old config
  l->waterStats->localWC(newPos1,NULL,true);
  l->waterStats->localWC(resn1->pos,resn1,true);
  l->waterStats->localWC(newPos2,NULL,true);
  l->waterStats->localWC(resn2->pos,resn2,true);
  
  //// new config
  l->waterStats->localWC(resn1->pos,NULL,false);
  l->waterStats->localWC(newPos1,resn1,false);
  l->waterStats->localWC(resn2->pos,NULL,false);
  l->waterStats->localWC(newPos2,resn2,false);
  
  Wstats deltaWC =  l->waterStats->calculateChangeInWC();


  int dE= newLocalStats.getDeltaE(oldLocalStats) + deltaWC.solvE;
  int mdE=0;
  double boltz;
  if(App::umbrellaSampling){
    mdE=dE + App::SpringConstant_Nint*( newLatticeStats.getNint()*newLatticeStats.getNint() - l->stats.getNint()*l->stats.getNint()+2*App::Q0*(l->stats.getNint()-newLatticeStats.getNint()))+
	App::SpringConstant_Cint*(newLatticeStats.getCint()*newLatticeStats.getCint() - l->stats.getCint()*l->stats.getCint()+2*App::C0*(l->stats.getCint()-newLatticeStats.getCint()));
    boltz=exp(((-(float)(mdE)))* l->getBetaMoves());
  }
  else{
    boltz = exp(((-(float) (dE)) )* l->getBetaMoves());
  }
  double bb = boltz/(boltz+1.0);
  if(isinf(boltz)){
    bb =1.0;
  }
  if(l->energyMap != NULL){
    l->energyMap->mapStats(l->stats,1.0 - bb,l->waterStats->totalE);
    l->energyMap->mapStats(newLatticeStats, bb,l->waterStats->totalE+dE);
  }
  bool accept=false;
  if(App::umbrellaSampling){
    if(mdE<=0 || drand48() < boltz){
      accept=true;
    }
  }
  else{
    if(dE<=0 || drand48() < boltz){
        accept=true;
      }
  }
  if(accept){

    //// update position and lattice 

    residues[n1]->pos = newPos1;
    residues[n2]->pos = newPos2;
    l->setResidue(newPos1,residues[n1]);
    l->setResidue(newPos2,residues[n2]);
    
    //// update lattice stats
    l->stats = newLatticeStats;
    
    //// WC
    l->waterStats->updateWC(deltaWC,dE); 
    l->waterStats->cleanUpWC();

    if(dE==0){
      return(1);
    }else{
      return(2);
    }
  }else{
    //// reset lattice
    l->setResidue(resn1->pos,resn1);
    l->setResidue(resn2->pos,resn2);

    //// WC
    l->waterStats->cleanUpWC();
    return(0);
  }


}


int Chain::globalMove(){
  if(drand48() < 0.5){
    return translate();
  }else{
    return rotatePoint();
  }

  return 0;
}


int Chain::translate(){
  int dir= (int)floor(6 * drand48());

  Pos posNew[MAX_RES];

  for(int n=0; n<N;n++){
    Residue * res = residues[n];
    posNew[n] = res->pos+ local[dir];
    posNew[n].periodicBoundary();
    
    Residue * testRes= l->getResidue(posNew[n]);
    //check for collision
    if(testRes!=NULL && testRes->chainNum != res->chainNum ){
#ifdef DEBUG
      cout << "clash with ligand "<<endl;;
#endif
      l->sampleCurrentStats();
      return -1;
    }  
  }

  //// clear all positions involved in move from lattice
  for(int n=0;n<N;n++){
    l->emptyLatticePos(residues[n]->pos);
  }

  //// calculate old and new stats
  //// calculate solvents stats of new for old and old for new, not including self
  oldLocalStats.clean();
  newLocalStats.clean();

  for(int n=0; n<N;n++){
    oldLocalStats.localStats(residues[n],residues[n]->pos ,l);
    oldLocalStats.solventStats(posNew[n],l);
    newLocalStats.localStats(residues[n],posNew[n],l);
    newLocalStats.solventStats(residues[n]->pos,l);
    
    //// Water Contacts
    
    //// old config
    l->waterStats->localWC(posNew[n],NULL,true);
    l->waterStats->localWC(residues[n]->pos,residues[n],true);
    //// new config
    l->waterStats->localWC(residues[n]->pos,NULL,false);
    l->waterStats->localWC(posNew[n],residues[n],false);


  }

  Wstats deltaWC =  l->waterStats->calculateChangeInWC();


  //// calculate lattice stats
  newLatticeStats.clean();
  newLatticeStats =  l->stats.delta(newLocalStats,oldLocalStats);
  int dE= newLocalStats.getDeltaE(oldLocalStats) + deltaWC.solvE;
  int mdE=0;
  double boltz;
  if(App::umbrellaSampling){
    mdE=dE + App::SpringConstant_Nint*( newLatticeStats.getNint()*newLatticeStats.getNint() - l->stats.getNint()*l->stats.getNint()+2*App::Q0*(l->stats.getNint()-newLatticeStats.getNint()))+
	App::SpringConstant_Cint*(newLatticeStats.getCint()*newLatticeStats.getCint() - l->stats.getCint()*l->stats.getCint()+2*App::C0*(l->stats.getCint()-newLatticeStats.getCint()));
    boltz=exp(((-(float)(mdE)))* l->getBetaMoves());
  }
  else{
    boltz = exp(((-(float) (dE)) )* l->getBetaMoves());
  }
  double bb = boltz/(boltz+1.0);
  if(isinf(boltz)){
    bb =1.0;
  }
  if(l->energyMap != NULL){
    l->energyMap->mapStats(l->stats,1.0 - bb,l->waterStats->totalE);
    l->energyMap->mapStats(newLatticeStats, bb,l->waterStats->totalE+dE);
  }
  bool accept=false;
  if(App::umbrellaSampling){
    if(mdE<=0 || drand48() < boltz){
      accept=true;
    }
  }
  else{
    if(dE<=0 || drand48() < boltz){
        accept=true;
      }
  }
  
  if(accept){
    // set positions residues
    // and set new lattice positions
    for(int n=0;n<N;n++){
      residues[n]->pos=posNew[n];
      l->setResidue(posNew[n],residues[n]);
    }
    l->stats = newLatticeStats;

    //// WC 
    l->waterStats->updateWC(deltaWC,dE); 
    l->waterStats->cleanUpWC();

    if(dE==0){
      return(1);
    }else{
      return(2);
    }
  }else{
    //// reset lattice
    for(int n=0; n<N;n++){
      l->setResidue(residues[n]->pos,residues[n]);
    }

    //// WC
    l->waterStats->cleanUpWC();
    return(0);
  }

}


int Chain::rotatePoint(){

  int rotationalDir= (int)floor(6 * drand48());
  int pivot= (int)floor(N * drand48());
  int startRes=0;
  int endRes=N-1;
  // OPT don't need to include pivot
  if(!frozen) {// if frozen only allow global moves
    if(drand48()<0.5){
      startRes= pivot;
    }else{
      endRes=pivot;
    }
  }
 
  Pos posNew[MAX_RES];
 
 // test for collisions
  for(int n=startRes; n<=endRes;n++){
    Residue * res = residues[n];
    posNew[n]= rotationPosition(res->pos,rotationalDir,residues[pivot]->pos);
#ifdef DEBUG
    cout<<"move to (" <<posNew[n].toString()<<")"<<endl;
#endif
    Residue * testRes= l->getResidue(posNew[n]);
    if(testRes!=NULL && (testRes->chainNum != res->chainNum || testRes->n<startRes || testRes->n> endRes  )){
      l->sampleCurrentStats();
      return -1;
    }  
  }
  
  oldLocalStats.clean();
  newLocalStats.clean();
  
  int exclStart =startRes;
  int exclEnd = endRes;
  if(startRes==pivot){
    exclStart = startRes +1;
  }else{
    exclEnd = endRes -1;
  }

  //// clear lattice positions involved in move
  for(int n=exclStart; n<=exclEnd;n++){
    l->emptyLatticePos(residues[n]->pos);
  }

  //// calculate old and new local stats
  for(int n=exclStart; n<=exclEnd;n++){
    Residue * res = residues[n];
    oldLocalStats.localStats(res,res->pos,l);
    newLocalStats.localStats(res,posNew[n],l);
    oldLocalStats.solventStats(posNew[n],l);
    newLocalStats.solventStats(res->pos,l);

    //// Water Contacts
    //// old config
    l->waterStats->localWC(posNew[n],NULL,true);
    l->waterStats->localWC(res->pos,res,true);
    //// new config
    l->waterStats->localWC(res->pos,NULL,false);
    l->waterStats->localWC(posNew[n],res,false);

  }

  //// calculate lattice stats
  newLatticeStats.clean();
  newLatticeStats =  l->stats.delta(newLocalStats,oldLocalStats);
  
  Wstats deltaWC =  l->waterStats->calculateChangeInWC();
  int dE= newLocalStats.getDeltaE(oldLocalStats) + deltaWC.solvE;
  int mdE=0;
  double boltz;
  if(App::umbrellaSampling){
    mdE=dE + App::SpringConstant_Nint*( newLatticeStats.getNint()*newLatticeStats.getNint() - l->stats.getNint()*l->stats.getNint()+2*App::Q0*(l->stats.getNint()-newLatticeStats.getNint()))+
	App::SpringConstant_Cint*(newLatticeStats.getCint()*newLatticeStats.getCint() - l->stats.getCint()*l->stats.getCint()+2*App::C0*(l->stats.getCint()-newLatticeStats.getCint()));
    boltz=exp(((-(float)(mdE)))* l->getBetaMoves());
  }
  else{
    boltz = exp(((-(float) (dE)) )* l->getBetaMoves());
  }
  double bb = boltz/(boltz+1.0);
  if(isinf(boltz)){
    bb =1.0;
  }
  if(l->energyMap != NULL){
    l->energyMap->mapStats(l->stats,1.0 - bb,l->waterStats->totalE);
    l->energyMap->mapStats(newLatticeStats, bb,l->waterStats->totalE+dE);
  }
  bool accept=false;
  if(App::umbrellaSampling){
    if(mdE<=0 || drand48() < boltz){
      accept=true;
    }
  }
  else{
    if(dE<=0 || drand48() < boltz){
        accept=true;
      }
  }
  
  if(accept){
    //// set new positions
    for(int n=startRes;n<=endRes;n++){
      residues[n]->pos=posNew[n];
      l->setResidue(posNew[n],residues[n]);
    }  
    l->stats = newLatticeStats;

    //// WC
    l->waterStats->updateWC(deltaWC,dE); 
    l->waterStats->cleanUpWC();

    if(dE==0){
      return(1);
    }else{
      return(2);
    }
  }else{
    //// reset old positions
    for(int n=startRes;n<=endRes;n++){
      l->setResidue(residues[n]->pos,residues[n]);
    }
    //// WC
    l->waterStats->cleanUpWC();
    return(0);
  }
}


Pos Chain::rotationPosition(Pos posOldi, int rotationDir,Pos posPivot){
  Pos output;
  int prim = rotationDir % 3;
  int sign =1;
  if(rotationDir >2){
    sign=-1;
  }
  Pos posOld =posOldi - posPivot ;
  switch(prim){
  case 0: // around x-axis
    output.x = posOld.x+ posPivot.x;
    output.y = sign*posOld.z + posPivot.y ;
    output.z= -1*sign*posOld.y + posPivot.z;
    break;
  case 1:
    output.x = -1*sign*posOld.z + posPivot.x;
    output.y = posOld.y + posPivot.y;
    output.z = sign*posOld.x + posPivot.z;
    break;
  case 2:
    output.x = sign*posOld.y + posPivot.x;
    output.y = -1*sign*posOld.x + posPivot.y ;
    output.z = posOld.z + posPivot.z;
    break;
  default:
    cout << "rotationCoord: wrong direction: "<< prim<<endl;
    exit(1);
  }
  output.periodicBoundary();
  return output  ;
}

int Chain::rotationCoord(int x,int y,int z, int dir, int rotationDir,int pivot){
  // OPT output 3D vector
  int prim = rotationDir % 3;
  int sign =1;
  if(rotationDir >2){
    sign=-1;
  }
  x -= residues[pivot]->pos.x;
  y -= residues[pivot]->pos.y;
  z -= residues[pivot]->pos.z;

  switch(prim){
  case 0: // around x-axis
    if(dir==XDIR) return x+ residues[pivot]->pos.x;
    else if(dir==YDIR) return ((sign*z + residues[pivot]->pos.y)  +LY)%LY;
    else return ((-1*sign*y + residues[pivot]->pos.z)   +LZ)%LZ;
    //break;
  case 1: // around y-axis
    if(dir==XDIR) return ((-1*sign*z + residues[pivot]->pos.x)+ LX)%LX;
    else if(dir==YDIR) return y + residues[pivot]->pos.y;
    else return ((sign*x + residues[pivot]->pos.z) + LZ)%LZ;
    //break;
  case 2: // around z-axis
    if(dir==XDIR) return ((sign*y + residues[pivot]->pos.x) +LX)%LX;
    else if(dir==YDIR) return ((-1*sign*x + residues[pivot]->pos.y) +LY)%LY;
    else return z + residues[pivot]->pos.z;
    //break;
  default:
    cout << "rotationCoord: wrong direction: "<< prim<<endl;
    exit(1);
   }
  return(-1);
}




int Chain::getCext(){
  int Cext=0;
  for(int n=0;n<N;n++){
    Residue * res= residues[n];
    //if(res->aa != 13){
      for(int k=0;k<6;k++){
	Pos posNB = local[k] + res->pos;
        posNB.periodicBoundary();
	Residue * neighbour =l->getResidue(posNB); 
	if(neighbour!=NULL && neighbour->chainNum != res->chainNum){
	  // if((neighbour->aa != 13)) Cext++;
	  Cext++;
	}
      }
      //}
  }
  return Cext;
}


bool Chain::hasCext(){
  for(int n=0;n<N;n++){
    Residue * res= residues[n];
    //if(res->aa != 13){
    for(int k=0;k<6;k++){
      Pos posNB = local[k] + res->pos;
      posNB.periodicBoundary();
      Residue * neighbour =l->getResidue(posNB); 
      if(neighbour!=NULL && neighbour->chainNum != res->chainNum){
	//  if((neighbour->aa != 13)) return true;
	return true;
      }
    }
  }
  return false;
}


