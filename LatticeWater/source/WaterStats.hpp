#ifndef _WaterStats_H_
#define _WaterStats_H_


#include "Residue.hpp"
#include <vector>

#define SURFACET0 0.41
#define SOLVATET0 0.49
#define SURALPHA (751*1.5)
#define SOLALPHA (701*1.5)
#define SURWEIGHT 0.41


using namespace std;


class Lattice;
class Chain;
class Stats;

struct Wstats{
  /// waterContacts
  int wcN;
  
  int wcHN;

  /// solvated residues
  int solvN;

  /// solvated hydrophobic residues
  int solvHN;
  
  /// solvation energy
  int solvE;

  /// solvated hydrophobic residues on the surface
  int surfaceHN;

  /// solvated fully hydrated residues on the surface
  int solvatedHN;

  
};




class WaterStats{
 public:
  /// holds global stats
  Wstats stats;
  
  /// total energy in system (+ Eext, Eint)
  int totalE;
  /// sets values to zero
  /// sets lattice
  void initGlobalWaterStats(Lattice *l);

  /// calculates and sets 'stats' variable
  void setGlobalWaterStats();
  
  /// adds chain to global stats
  void addChain2Global(Chain *c, Stats & chainStats);

  /// subtracts chain from global stats
  void subtractChainGlobal(Chain *c, Stats & chainStats);

  /// cleans all local variables for residues in chain
  void initChainWC(Chain * c);
  
  /// calculates waterContacts in chain
  Wstats getWaterStats(Chain *c);

  /// calculates change in Wstats for AA swap
  Wstats dEAASwap(const Pos & pos, int oldaa, int newaa);
  
  Wstats dEAAUnfoldSwap(const Pos & pos, int oldaa, int newaa);

  /// sets localOldWC, localNewWC for affected residues
  /// if res==NULL, the position is empty
  void localWC(const Pos & pos, Residue * res, bool old); 
 
  /// calculates the change in water contacts on proposed moves
  /// localOldWC, localNewWC should already be set
  Wstats calculateChangeInWC();

  /// updates affected residues
  void updateWC(Wstats change,int changeEtotal);


  /// cleans all local variables for residues affected
  void cleanUpWC();


  bool checkWaterStats();
  
  void printWstats(Wstats & in);
   
  void printWstats(); 

  string print2string();

private:
  /// keeps residues affected by current move
  /// may contain multiple copies of a residue
  vector<Residue *> affectedResidues;

  Wstats calculateGlobalWaterStats();
  
  Wstats sumGlobalWaterStats();   

  Lattice *l;
 
}; 




#endif
