#include "Stats.hpp"
#include "StatsMethods.hpp"
#include "Lattice.hpp"
#include "Native.hpp"
#include "AA.hpp"
#include "App.hpp"

#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>



int getInteraction(int,int);

int getTempInteraction(int,int,double);


void Stats::getLatticeStats(Lattice * l){
  Eext=0;
  Eint=0;
  Cint=0;
  Cext=0;
  Ctot=0;
  Etot=0;
  Esol =0;

  Nint=0;
  Next=0;
  Ntot=0;

  for(int cn=0;cn< l->nChains ; cn++){
    Chain * c=l->chains[cn];
    for(int n=0;n< c->N;n++){
      Residue * res= c->residues[n];
      Stats tmp;
      //tmp.localStats(res,res->pos,res->spin,l);
      tmp.localStats(res,res->pos,l);
      (*this) += tmp;
    }
  }
  Eext /=2 ;
  Eint /=2;
  Cint /=2;
  Cext /=2;

  Etot=Eext+Eint+Esol;
  Ctot=Cint+Cext;

 
  Nint /=2;
  Next /=2;
  Ntot=Nint+Next;
  //if(App::umbrellaSampling)
   // Etot=Etot+App::SpringConstant*(Nint-App::Q0)*(Nint-App::Q0);

}












void Stats::get_Eint_Cint(Chain*  c, Lattice *l){
  Eint =0;
  Cint =0;
  Esol =0 ;
  for(int n=0;n<c->N;n++){
    Residue * res= c->residues[n];
    Stats tmp;
    tmp.localStats(res,res->pos,l);
    (*this) += tmp;
  }
  Eint /=2;
  Cint /=2;
  Etot = Eint+Esol;
  Ctot = Cint;

  // set all others to zero
  Eext=0;
  Cext=0;

  Nint/=2;  
  Next/=2;
  Ntot=Nint;
  //if(App::umbrellaSampling)
   // Etot=Etot+App::SpringConstant*(Nint-App::Q0)*(Nint-App::Q0);
}









void Stats::printCout(){
  cout<<"Eint "<< Eint;
  cout<<" Eext "<< Eext;
  cout<<" Esol "<<Esol;
  cout<<" Etot "<< Etot<<endl;

  cout<<"Cint "<<Cint;
  cout<<" Cext "<<Cext;
  cout<<" Ctot "<<Ctot<<endl;
  

  if(App::nativeStats){ 
    cout<<"Nint "<<Nint;
    cout<<" Next "<<Next;
    cout<<" Ntot "<<Ntot<<endl;
  }
}

string Stats::print2string(){
  std::stringstream myStream;
  myStream<<"Eint "<< Eint;
  myStream<<" Eext "<< Eext;
  myStream<<" Esol "<< Esol;
  myStream<<" Etot "<< Etot<<endl;

  myStream<<"Cint "<<Cint;
  myStream<<" Cext "<<Cext;
  myStream<<" Ctot "<<Ctot<<endl;
  
  if(App::nativeStats){	 
    myStream<<"Nint "<<Nint;
    myStream<<" Next "<<Next;
    myStream<<" Ntot "<<Ntot<<endl;
  }

  return myStream.str();
}




