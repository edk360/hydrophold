#!/bin/sh

time mpirun -np 8 ../LatticeWater/source/grancan -noSwap   -HalfPotential   -i PDB/P1_Fixed.pdb -native PDB/P1_Fixed.pdb -minT 0.2 -maxT 0.9 -S 5000 -W 1000 -aa input/aa_water2.txt -lT  -indxWater 20 -findLowestE > output.out 
