#ifndef _Residue_H_
#define _Residue_H_

#include "Pos.hpp"


#define XDIR 0
#define YDIR 1
#define ZDIR 2

enum State {inCoil,inStrand};

extern Pos local[6];

struct Residue{
  int aa;
  Pos pos;
  //Pos spin;
  //Pos bkwd;
  //Pos fwd;
  int n; // number in chain
  int chainNum;
  State state;
  /// WaterContact variables
  int waterContacts;
  int localOldWC;
  int localNewWC;
  int newWC;
  bool visitedWC;

};


#endif
