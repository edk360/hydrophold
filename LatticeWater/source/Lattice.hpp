#ifndef _Lattice_H_
#define _Lattice_H_

#define MAX_CHAINS 50000


#include "Residue.hpp"
#include "Stats.hpp"
#include "EnergyMap.hpp"
#include "WaterStats.hpp"


#include <math.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string.h>

using namespace std;

class Chain;
class Stats;
class Native;
class EnergyMap;
class AA;
class MolBox;
class WaterStats;
struct Config;

class Lattice{
public:
  Lattice();
  void init();
  Residue * getResidue(const Pos & p);
  void emptyLatticePos(const Pos & p);
  void setResidue(const Pos &  p,Residue * r);

  void readPDBMultiChain(string s);
  void oldReadPDBMultiChain(string s);
  void writePDBMultiChain(string s);
  void writePDBMultiChainNatives(string s);
  void printPeriodicPDB();
  int insertChain(Config * config, int * sequence, double beta);
  int deleteChain(int chainNum);
  void setAA(string fn, int water_indx);
  bool checkStats();
  double getBetaMoves();
  double getRealBeta();
  void setBetaMoves(double b);
  void sampleCurrentStats();
  void setNative(string fn_native);
  
  Chain * chains[MAX_CHAINS];
  Residue * r[LX][LY][LZ];
  Stats stats;
  int nChains;
  Native * native;
  EnergyMap * energyMap;
  AA * aaInt;

  /// Water Contacts 
  WaterStats * waterStats; 
  
private:
  double betaMoves;
  //int freeChains;

 
};



inline
Residue * Lattice::getResidue(const Pos & p){
  return r[p.x][p.y][p.z];
}


inline
void Lattice::emptyLatticePos(const Pos & p){
  r[p.x][p.y][p.z]=NULL;
}

inline
void Lattice::setResidue(const Pos & p,Residue * res){
  r[p.x][p.y][p.z]=res;
}


inline
void Lattice::setBetaMoves(double m){
  betaMoves=m;
}


inline 
double Lattice::getBetaMoves(){
  return betaMoves;
}

inline 
double Lattice::getRealBeta(){
  return 100*betaMoves;
}


inline
void Lattice::sampleCurrentStats(){
  if(energyMap){
    energyMap->mapStats(stats,1.0,waterStats->totalE);
  }
}

#endif
