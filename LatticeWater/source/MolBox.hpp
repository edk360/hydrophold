#ifndef _MolBox_hpp_
#define _MolBox_hpp_

#include "Pos.hpp"


#define MAXN 100
#define MAX_CONFIGS 10000
#define EQUIL_STEPS 10000


class Chain;
class Lattice;

struct Config{
  Pos positions[MAXN];
  int nTotal;
};


class MolBox{
public:
  MolBox(Lattice * mainLattice);
  void setMolecule(Chain * chain);
  void createNewSet(double beta,int num_configs, bool betaChanged);
  void  printConfig(int c);
  Config * getNewConfig();
  void setAA(string fn, int water_indx);
  double getBeta();
  int sequence[MAXN];
private:
  void recordConfig(int insertAt);
  void equilibrate(int nsteps);
  //  double beta;
  int currentConfig;
  int numConfigs;
  Config * configSet[MAX_CONFIGS];
  Lattice * box;                       // perhaps need to make this box smaller
  Chain * molecule;
  Lattice * mainLattice;
};


#endif






