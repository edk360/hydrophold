#ifndef _AADISTR_HPP_
#define _AADISTR_HPP_

#include <string.h>
#include <stdio.h>
#include <iomanip>

#include "AA.hpp"

using namespace std;

class Lattice;

class AADistr{
public:
  AADistr();
  void   init(string filename, AA * aaInt);
  
  void setSequence(Lattice *l);
  double getDistance();
  double getDistanceSubst(int aa1, int aa2);
  
  /// updates freqency table
  void substitute(int aa1, int aa2,double dist);

  string toString();
  bool check();

private:
  /// frequencies as observed in PDB (from file)
  double freqPDB[MAXAA];
  /// expected frequencies given the sequence length
  double freqExp[MAXAA];
  /// frequencies in current sequence
  double freqSeq[MAXAA];
  void setLength(int length);
  int seqLength;
  int numaa;
  AA * aaInt;
  double systemDistance;
  double sumFreq;
};


inline
void AADistr::substitute(int aa1, int aa2, double dist){
  freqSeq[aa1]--;
  freqSeq[aa2]++;
  systemDistance += dist;
}

inline
double AADistr::getDistanceSubst(int aa1, int aa2){
  if(aa1==aa2)return 0;
  return 2*((freqExp[aa1] - freqSeq[aa1])- (freqExp[aa2] - freqSeq[aa2]-1.0)); 
}

#endif
