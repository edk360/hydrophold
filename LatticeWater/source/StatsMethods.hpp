#ifndef _Stats_Methods_H_
#define _Stats_Methods_H_


#include "Stats.hpp"
#include "Lattice.hpp"
#include "Pos.hpp"
#include "AA.hpp"
#include "Chain.hpp"
#include "Native.hpp"
#include "App.hpp"

#include <cmath>
#include <cstdlib>
/////////////////////////////////////////////////////////////////////////
inline
void Stats::clean(){
  Esol=0;
  Eext=0;
  Eint=0;
  Etot=0;
  Cint=0;
  Cext=0;
  Ctot=0;
 
  Nint=0;
  Next=0;
  Ntot=0;
};


inline
Stats::Stats(){
  Esol=0;
  Eext=0;
  Eint=0;
  Etot=0;
  Cint=0;
  Cext=0;
  Ctot=0;

  Nint=0;
  Next=0;
  Ntot=0;

};


inline
Stats Stats::operator+ (const Stats& s) const{
  Stats result;
  result.Cint = (Cint + s.Cint);
  result.Cext = (Cext + s.Cext);
  result.Ctot = (Ctot + s.Ctot);
  result.Eint = (Eint + s.Eint);
  result.Eext = (Eext + s.Eext);
  result.Etot = (Etot + s.Etot);
  result.Esol = (Esol + s.Esol);
if(App::nativeStats){
  result.Nint = (Nint + s.Nint);
  result.Next = (Next + s.Next);
  result.Ntot = (Ntot + s.Ntot);
 }
  return result;
}


inline
Stats & Stats::operator+= (const Stats& s){
  Stats result;
  Cint += ( s.Cint);
  Cext += ( s.Cext);
  Ctot += ( s.Ctot);
  Eint += ( s.Eint);
  Eext += ( s.Eext);
  Etot += ( s.Etot);
  Esol += ( s.Esol);
if(App::nativeStats){
  Nint += ( s.Nint);
  Next += ( s.Next);
  Ntot += ( s.Ntot);
 }
  return (*this);
}

inline
Stats & Stats::operator-= (const Stats& s){
  Stats result;
  Cint -= ( s.Cint);
  Cext -= ( s.Cext);
  Ctot -= ( s.Ctot);
  Eint -= ( s.Eint);
  Eext -= ( s.Eext);
  Etot -= ( s.Etot);
  Esol -= ( s.Esol);
if(App::nativeStats){
  Nint -= ( s.Nint);
  Next -= ( s.Next);
  Ntot -= ( s.Ntot);
 }
  return (*this);
}

inline
Stats & Stats::operator=(const Stats & s){
  Cint =  s.Cint;
  Cext =  s.Cext;
  Ctot =  s.Ctot;
  Eint =  s.Eint;
  Eext =  s.Eext;
  Etot =  s.Etot;
  Esol =  s.Esol;
if(App::nativeStats){
  Nint =  s.Nint;
  Next =  s.Next;
  Ntot =  s.Ntot ;
 }
  return (*this);
}

inline
const bool  Stats::operator != (const Stats& s) const{
  bool ans = (Cint !=  s.Cint ||
	      Cext !=  s.Cext ||
	      Ctot !=  s.Ctot ||
	      Eint !=  s.Eint ||
	      Eext !=  s.Eext ||
	      Etot !=  s.Etot ||
	      Esol !=  s.Esol );
  if(App::nativeStats){
    ans = (ans ||
	   Nint !=  s.Nint ||
	   Next !=  s.Next ||
	   Ntot !=  s.Ntot );
  }
  return ans;
}

inline
Stats  Stats::delta(const Stats & Snew, const  Stats & Sold)const{  
  Stats out; 
  out.Eext = Eext + Snew.Eext - Sold.Eext;
  out.Eint = Eint + Snew.Eint - Sold.Eint;
  out.Etot = Etot + Snew.Etot - Sold.Etot;
  out.Esol = Esol + Snew.Esol - Sold.Esol;

  out.Cext = Cext + Snew.Cext - Sold.Cext;
  out.Cint = Cint + Snew.Cint - Sold.Cint;
  out.Ctot = Ctot + Snew.Ctot - Sold.Ctot;

if(App::nativeStats){	 
  out.Next = Next + Snew.Next - Sold.Next;
  out.Nint = Nint + Snew.Nint - Sold.Nint;
  out.Ntot = Ntot + Snew.Ntot - Sold.Ntot;
 }
 return out;
}

inline
//void Stats::localStats(Residue * res,const Pos & pos, const Pos & spin, Lattice * l){
void Stats::localStats(Residue * res,const Pos & pos, Lattice * l){
  localStatsAA(res,pos,res->aa,l);
}


inline
//void Stats::localStats(Residue * res,const Pos & pos, const Pos & spin, Lattice * l){
void Stats::localStatsAA(Residue * res,const Pos & pos,int aa, Lattice * l){
  for(int k=0;k<6;k++){
    Pos posNB =local[k]+ pos;
    posNB.periodicBoundary();
    Residue * resNB = l->getResidue(posNB);
    if(resNB!=NULL){
      if(resNB->chainNum!=res->chainNum){
	Eext+=  l->aaInt->getInteraction(aa, resNB->aa);
#ifdef AGGR
	if((aa != 13) && (resNB->aa != 13)) Cext++; // dont count threonine contacts
#else
	Cext++;
#endif
	
	if(App::nativeStats){
	  if(l->native->isNativeContact(res->chainNum,resNB->chainNum,res->n,resNB->n)){
	    Next++;
	  }
	}
      }else if(abs((int) (resNB->n - res->n))!=1) {
	Eint+=  l->aaInt->getInteraction(aa, resNB->aa);
	Cint++;
	if(App::nativeStats){
	  if(l->native->isNativeContact(res->chainNum,resNB->chainNum,res->n,resNB->n)){
	    Nint++;
	  }
	}
      }
    }else{ //// Water contact
      //// check if water interactions are set
      /*if(AA::WATER >= 0){
	//// add water interaction
	Esol +=  l->aaInt->getInteraction(aa, AA::WATER)/4;
	}*/
    }
  }
  Etot=Eext+Eint+Esol;
  Ctot=Cint+Cext;
  if(App::nativeStats){
    Ntot=Nint+Next;
  }
}

inline
//void Stats::localStatsExclude(Residue * res,const Pos & pos, const Pos & spin, Lattice * l,int start,int end){
void Stats::localStatsExclude(Residue * res,const Pos & pos, Lattice * l,int start,int end){
 for(int k=0;k<6;k++){
    Pos posNB =local[k]+ pos;
    posNB.periodicBoundary();
    Residue * resNB = l->getResidue(posNB);
    if(resNB!=NULL){
      if(resNB->chainNum!=res->chainNum){
	Eext+=  l->aaInt->getInteraction(res->aa, resNB->aa);
#ifdef AGGR
	if((res->aa != 13) && (resNB->aa != 13)) Cext++; // dont count threonine contacts
#else
	Cext++;
#endif
	
	if(App::nativeStats){
	  if(l->native->isNativeContact(res->chainNum,resNB->chainNum,res->n,resNB->n)){
	    Next++;
	  }
	}
      }else if(abs(resNB->n - res->n)!=1) {
	if((resNB->n <start) || (resNB->n > end)){
	  Eint+=  l->aaInt->getInteraction(res->aa, resNB->aa);
	  Cint++;
	}
	if(App::nativeStats){
	  if(l->native->isNativeContact(res->chainNum,resNB->chainNum,res->n,resNB->n)){
	    Nint++;
	  }
	}
      }
    }else{ //// Water contact
      //// check if water interactions are set
      /*if(AA::WATER >= 0){
	//// add water interaction
	Esol +=  l->aaInt->getInteraction(res->aa, AA::WATER)/4;
	}*/
    }
  }
  Etot=Eext+Eint +Esol;
  Ctot=Cint+Cext;
  if(App::nativeStats){
    Ntot=Nint+Next;
  }
}


inline
int Stats::getDeltaE(const Stats & old)const{
  return Etot -old.Etot;
}



inline
void Stats::solventStats(Pos & pos,Lattice * l){
  /*if(AA::WATER < 0)return;
  int Es=0;
  for(int k=0;k<6;k++){
    Pos posNB =local[k]+ pos;
    posNB.periodicBoundary();
    Residue * resNB = l->getResidue(posNB);
    if(resNB!=NULL){  
      Es +=   l->aaInt->getInteraction(resNB->aa, AA::WATER)/4 ;
    }
  }
  Esol += Es;
  Etot += Es;*/
}




#endif
