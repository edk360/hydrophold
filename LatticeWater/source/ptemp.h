#ifndef _ptemp_h_
#define _ptemp_h_


#define MAX_PROCS 16


extern double betas[MAX_PROCS];
extern int myRank;
extern bool swapT;
extern bool LAMBDA_COR;

#endif
