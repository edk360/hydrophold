#ifndef _Native_H_
#define _Native_H_


#include "Lattice.hpp"
#include "Chain.hpp"

#ifdef NATIVE
class Native{
public:
  Native(Lattice *l);
  int contacts[MAX_CHAINS][MAX_CHAINS][MAX_RES][MAX_RES];
  int totCnat;
};

#endif
#endif
