#ifndef _Chain_H_
#define _Chain_H_

#include "Residue.hpp"
#include "Stats.hpp"

#include <vector>

#define MAX_RES 350

#define StateE 0.0
#define StericE 0.5
#define HBondE -1.0

using namespace std;

class Lattice;



class Chain{
public:
  Chain(Lattice *l,int chnum);
  ~Chain();
  int localMove();
  int globalMove();
  int rotatePoint();
  void setChainNum(int n);
  int getCext();
  bool hasCext();
  void setRandomSpins();
  void setBkwdFwdSpins();
  int shuffleSpin();
  void setStateResidues();
  int N;
  Residue * residues[MAX_RES];
  bool frozen;
  bool locked;

private:
  Lattice * l;
  int crankshaft(int n1, int n2,int px, int py, int pz);
 
  int translate();
  int rotationCoord(int x,int y,int z, int dir, int rotationDir,int pivot);
  Pos rotationPosition(Pos posOld, int rotationDir,Pos posPivot);
  int chainNum;

  /// Variable to be used in calculation of stats
  Stats oldLocalStats;
  /// Variable to be used in calculation of stats
  Stats newLocalStats;
  /// Variable to be used in calculation of stats
  Stats newLatticeStats;


  
  

};


#endif
