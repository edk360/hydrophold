#ifndef _MC_H_
#define _MC_H_

#include <string.h>

#include <iostream>

using namespace std;


int initMC(int argc,char *argv[]);
int MC(int ksteps, double beta, int betaID);
void getMCStats(int num_procs, int myRank);


extern double minBeta;
extern double maxBeta;
extern int total_swaps;
extern string fn_aa;

#endif
