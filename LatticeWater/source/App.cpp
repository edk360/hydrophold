#include "App.hpp"
#include "GranCan.hpp"
#include "Design.hpp"
#include "ptemp.h"
#include "MolBox.hpp"
#include "Lattice.hpp"
#include "Stats.hpp"
#include "StatsMethods.hpp"

#include <string>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>



using namespace std;

string App::fn_aa ="aa.txt";
int App::indx_water=-1;

double App::minBeta=0.01;
double App::maxBeta=0.05;

string App::fn_stats="stats_";
string App::fn_eMap ="eMap_";
//string App::fn_hbond ="hbond_";

string App::fn_in = "RWWLY10_solo.pdb";
string App::fn_native="";
string App::fn_AAdistribution="";

bool App::TempDepPotential=false;
bool App::nativeStats=false;
bool App::linear_T=false;
bool App::get_Cext=false;
bool App::get_PeriodicPDB=false;
bool App::LAMBDA_COR=false;
bool App::useOldFormat=false;
bool App::designProgram=false;
bool App::AAdistribution=false;
bool App::checkBelowNative=false;
bool App::findLowestE=false;
bool App::writeCluster=false;
bool App::writeCluster5=false;
bool App::umbrellaSampling=false;
bool App::NewPotential=false;
bool App::HalfPotential=false;
bool App::SeparateT0=false;

bool App::hbondStats=false;
bool App::cintStats=false;

bool App::setAir=false;

double  App::eBetaMu = -0.0000005;
double App::alpha=0.;
double App::T0=0;

ofstream App::moviestream;
bool App::recordMovie=false;
int App::moviestep=0;
int App::num_configs= 10000;
int App::tableStep=0;
int App::Q0=0;
int App::C0=0;
int App::SpringConstant_Nint=0;
int App::SpringConstant_Cint=0;

double App::designT;
bool App::DeltaDesign=false;

GranCan * App::grancan=NULL;

void App::getOpts(int argc,char *argv[]){
  grancan= new GranCan();
  
  double minT=0,maxT=0;

  int optind=1;
#ifdef CROSSNBS
  Stats::setLoopUpCrossNbs();
#endif
  while ((optind < argc) && (argv[optind][0]=='-')) {
    string sw = argv[optind];
    //cout<<"* "<< sw <<endl;
    if (sw=="-minB") {
      optind++;
      minBeta = atof(argv[optind]);
    }else if(sw=="-maxB") {
      optind++;
      maxBeta = atof(argv[optind]);
    }else if (sw=="-minT") {
      optind++;
      minT = atof(argv[optind]);
    }else if(sw=="-maxT") {
      optind++;
      maxT = atof(argv[optind]);
    }else if(sw=="-S"){
      optind++;
      grancan->steps = atoi(argv[optind]);
    }else if(sw=="-W"){
      optind++;
      grancan->total_swaps = atoi(argv[optind]);
    }else if(sw=="-Q0"){
      optind++;
      Q0=atoi(argv[optind]);
    }else if(sw=="-C0"){
      optind++;
      C0=atoi(argv[optind]);
    }else if(sw=="-SpringConstantNint"){
      optind++;
      SpringConstant_Nint=atoi(argv[optind]);
    }
    else if(sw=="-SpringConstantCint"){
      optind++;
      SpringConstant_Cint=atoi(argv[optind]);
    }
    else if(sw=="-i"){
      optind++;
      fn_in.assign(argv[optind]);
    }else if(sw=="-oldFormat"){
      optind++;
      useOldFormat=true;
      fn_in.assign(argv[optind]);
    }else if (sw=="-ebmu") {
      optind++;
      eBetaMu = atof(argv[optind]);
    }else if(sw=="-alpha"){
      optind++;
      alpha=atof(argv[optind]) * 100;
    }else if(sw=="-T0"){
      optind++;
      T0=atof(argv[optind]);
    }
    else if(sw=="-aa"){
      optind++;
      cout<< argv[optind]<<endl;
      fn_aa.assign(argv[optind]);
    }else if(sw=="-indxWater"){
      optind++;
      indx_water= atoi(argv[optind]);
    }else if(sw=="-TempDepPotential"){
      TempDepPotential=true;
    }else if(sw=="-setAir"){
      setAir=true;
    }else if(sw=="-native"){
      optind++;
      fn_native.assign(argv[optind]);
      nativeStats=true;
    }else if(sw=="-lT"){
      linear_T=true;
    }else if(sw=="-noSwap"){
      swapT=false;
    }else if(sw=="-getCext"){
      get_Cext=true;
    }else if(sw=="-getPeriodicPDB"){
      get_PeriodicPDB=true;
    }else if(sw=="-lambdaCor"){
      LAMBDA_COR=true;
    }else if(sw=="-movie"){
      recordMovie=true;
    }else if(sw=="-checkBelowNative"){
      checkBelowNative=true;
    }else if(sw=="-findLowestE"){
      checkBelowNative=true;
      findLowestE=true;
    }else if(sw=="-writeCluster"){
      writeCluster=true;
    }else if(sw=="-writeCluster5"){
      writeCluster5=true;
    }else if(sw=="-hbondStats"){
      hbondStats=true;
    }else if(sw=="-CintStats"){
      cintStats=true;
    }else if(sw=="-AAdistr"){
      AAdistribution=true;
      optind++;
      fn_AAdistribution.assign(argv[optind]);
    }else if(sw=="-DeltaDesign"){
      DeltaDesign=true;
    }else if(sw=="-umbrellaSampling"){
      umbrellaSampling=true;
    }else if(sw=="-NewPotential"){
       NewPotential=true;
    }else if(sw=="-HalfPotential"){
        HalfPotential=true;
     }else if(sw=="-SeparateT0"){
    	 SeparateT0=true;
      }


    else if(sw=="-designT"){
      designProgram=true;
      optind++;
      designT = atof(argv[optind]);
    }else{
      cout << "unknown option: "<< sw<<endl;
      exit(1);
    }
    optind++;
  } 
 
  if(minT >0){maxBeta= 0.01/minT;cout<<"minBeta "<<maxBeta<<endl;}
  if(maxT >0){minBeta= 0.01/maxT;cout<<"maxBeta "<<minBeta<<endl;}
}

void App::init(double myBeta)
{
  if(designProgram){
    Design * des= new Design();
    cout<<"a"<<endl;
    if(! useOldFormat){
      cout<<"a1"<<endl;
      des->init(fn_in);
    }else{
      cout<<"not implemented design with old format"<<endl;
      exit(1);
      // des->initOldFormat(fn_in);
    }
    cout<<"b"<<endl;
    des->designProcedure((minBeta + maxBeta)/2.0, grancan->steps);
    exit(0);
  }
  grancan->mainLattice->setBetaMoves(myBeta);
  grancan->mainLattice->energyMap->initTableStats();


  num_configs= (int)(grancan->steps * grancan->pInsDel * 1.1);
  if(num_configs > MAX_CONFIGS)num_configs =MAX_CONFIGS;
  cout<< "num_configs per set: "<<  num_configs<< endl;
  if(eBetaMu<0){grancan->pInsDel=0.0;}
 
  grancan->molbox->setAA(fn_aa,indx_water);
  grancan->mainLattice->setAA(fn_aa,indx_water);
  if(! useOldFormat){
    grancan->mainLattice->readPDBMultiChain(fn_in);
  }else{
    grancan->mainLattice->oldReadPDBMultiChain(fn_in);
  }

  if(nativeStats){
     if(fn_native== ""){
       fn_native = fn_in;
     }
     grancan->mainLattice->setNative(fn_native);
     cout<< "set native "<<endl;
  }

  cout<< "read file "<<fn_in<<endl;
  
  /*if(setAir){
    grancan->mainLattice->setSolvent();
    //recalculate stats
    grancan->mainLattice->resetStats();
    }*/

  if(eBetaMu>0){
    grancan->molbox->setMolecule(grancan->mainLattice->chains[0]);
    cout<< "molbox molecule set "<<endl;
  }
 
  
  if(get_Cext){
    grancan->getCext();
    exit(0);
  }
  if(get_PeriodicPDB){
    grancan->mainLattice->printPeriodicPDB();
    exit(0);
  }

  string fn_movie ="movie.pdb";
  if(recordMovie){
    moviestream.open(fn_movie.c_str());
  }
  Stats newStats;
  newStats.getLatticeStats(grancan->mainLattice);
  cout<< "intial stats:"<<endl;
  newStats.printCout();
  //mainLattice->checkStats();

  //return(0);
}



