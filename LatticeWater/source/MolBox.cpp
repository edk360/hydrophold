#include "MolBox.hpp"

#include "Chain.hpp"
#include "Lattice.hpp"
#include "Native.hpp"
#include "Stats.hpp"
#include "WaterStats.hpp"
#include "App.hpp"

#include <cstdlib>

//#define DEBUG

MolBox::MolBox(Lattice * mainL){
  //beta = 0.08;
  currentConfig =0;
  numConfigs =1000;
  box = new Lattice;
  box->init();
  mainLattice = mainL;
  box->setBetaMoves(mainLattice->getBetaMoves());
  for(int i =0;i<MAX_CONFIGS;i++){
    configSet[i]=NULL;
  }
  box->waterStats->initGlobalWaterStats(box);
}

void MolBox::setAA(string fn, int water_indx ){
  box->setAA(  fn,  water_indx );
}

/*void MolBox::setBeta(double b){
  beta=b;
  }*/

double MolBox::getBeta(){
  return box->getBetaMoves();
}


void MolBox::setMolecule(Chain * chain){
  // lattice should be clean 

  molecule = new Chain(box,0);
  box->chains[0]=molecule;
  molecule->frozen = false;
  molecule->locked = false;
  molecule->N = chain->N;
  
  // copy and insert each residue
  for(int n=0;n<chain->N;n++){
    Residue * res = new Residue;
    res->pos.x = chain->residues[n]->pos.x;
    res->pos.y =chain->residues[n]->pos.y;
    res->pos.z =chain->residues[n]->pos.z;
    res->aa =chain->residues[n]->aa;
    sequence[n]=res->aa;
    res->n =chain->residues[n]->n;
    res->chainNum=0;  
    molecule->residues[n]=res;
    box->r[res->pos.x][res->pos.y][res->pos.z] = molecule->residues[n];
  }
 
  box->waterStats->initChainWC(molecule);
  box->waterStats->setGlobalWaterStats();

  box->nChains = 1;
  if(App::nativeStats){
    box->native = new Native(box);
  }
  box->stats.getLatticeStats(box);
  box->energyMap = NULL;// should not record eMap
}



void MolBox::createNewSet(double b,int num_configs, bool betaChanged){
  // need to set Stats::l
#ifdef DEBUG
  cout<< "CREATING NEW SET "<<endl;
#endif
  box->setBetaMoves(b);
  //molecule->setBeta(beta);
  if(betaChanged){
    //cout<<"beta changed"<<endl;
    equilibrate(EQUIL_STEPS);
  }
  
  currentConfig=0;
  if(num_configs > MAX_CONFIGS)num_configs = MAX_CONFIGS;
  numConfigs=num_configs;
 
  for(int nc =0;nc<numConfigs;nc++){
    // cout<< nc <<endl;
    equilibrate((int) floor(10 * drand48()));
    recordConfig(nc);         
  } 
#ifdef DEBUG
  cout<< "NEW SET CREATED"<<endl;
#endif
}

void  MolBox::printConfig(int c){
  Config * config = configSet[c];
  cout<<"configuration"<< c<<endl;
  for(int n=0;n<config->nTotal;n++){
    cout<<"config res:"<<n<<" "<<config->positions[n].x<<" "<<config->positions[n].y<<" "<<config->positions[n].z<<endl;
  }
}



Config * MolBox::getNewConfig(){
  currentConfig++;
  if(currentConfig >= numConfigs){
    createNewSet(box->getBetaMoves(),numConfigs, false);
  }
  // perhaps better to choose @random?
  int c = (int) floor((numConfigs-1) * drand48());
  //cout<<"config no: "<<c<<endl;
  return configSet[c ];  
}
 

void MolBox::equilibrate(int nsteps){
  for(int i =0;i<nsteps;i++){
    molecule->localMove();
    if(drand48() < 0.05 ){
      molecule->rotatePoint();
    }
  }
}


void MolBox::recordConfig(int insertAt){
  //cout<<"recording config"<<endl;
  if(configSet[insertAt] !=NULL){
    delete configSet[insertAt];
    configSet[insertAt] = NULL;
  }
  
  Config * cc = new Config;
 
  for(int n=0; n<molecule->N;n++){
    // perhaps needs normalising ?

    cc->positions[n] = molecule->residues[n]->pos;

    cc->nTotal = molecule->N;
  }

  configSet[insertAt]= cc;
}

