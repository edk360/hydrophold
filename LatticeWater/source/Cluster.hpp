#ifndef _Cluster_H_
#define _Cluster_H_

#include "Lattice.hpp"

#define MAX_CLUST 1000


using namespace std;

class Cluster{
public:
  Cluster();
  void startCounting(Lattice * l1);
  void printCout();
  ClusterInfo checkCluster(Lattice *l,double beta);
  //int checkCluster(Lattice *l,double beta, int required_cluster_size);
private:
  void writePDBClust(int clust, double beta);
  void visit(int chain, int cluster);
  void reset();
  int clusterCount[MAX_CLUST];
  int totalClusters;
  int visited[MAX_CHAINS];
  int totalVisited;
  static  int oldClusterSize;
  int largestCluster;
  Lattice *l;
};

#endif
