#include <mpi.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <cstdlib>

#include "GranCan.hpp"
#include "ptemp.h"
#include "App.hpp"
#include <unistd.h>
#include <time.h>
//#define DEBUG
using namespace std;
//using namespace __gnu_cxx;

//mpiCC tstmpi.cpp -o tstmpi



// MESSAGE TYPES
#define SWAP_REQUEST 1
#define ALL_DONE 2



// FORWARD DECLARATIONS

bool trySwap(int,int);
int initBetas(double minBeta,double maxBeta);

// GLOBALS
int MASTER =0;
int myRank;
bool LAMBDA_COR = false;

double betas[MAX_PROCS];
int betaID2node[MAX_PROCS];
int node2betaID[MAX_PROCS];
int energies[MAX_PROCS];
int nChains[MAX_PROCS];



double myBeta;
int num_procs;

bool swapT=true;

//Rates * transitionRates;

//double fraction_swaps=0.01;


int main(int argc, char *argv[]) {

  //int steps=0;
  char name[MPI_MAX_PROCESSOR_NAME];
  int namelen;
  int seed;
  


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI::Get_processor_name(name, namelen);
  //MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  myRank = MPI::COMM_WORLD.Get_rank ( );

  if(num_procs > MAX_PROCS){
    cout<< "too many processes: "<<num_procs;
    exit(1);
  }
  printf("Process %d out of %d realp %s \n", myRank, num_procs,name);
  seed = getpid()^time(0);
  cout << "Random seed: " << seed << endl;

  srand48(myRank^getpid());
  // initMC(argc, argv);
  App::getOpts(argc, argv);
  if(myRank==MASTER){
    initBetas(App::minBeta,App::maxBeta);
  }
  MPI::COMM_WORLD.Bcast(&node2betaID,num_procs,MPI::INT, MASTER);
  MPI::COMM_WORLD.Bcast(&betas,num_procs,MPI::DOUBLE, MASTER);
  MPI::COMM_WORLD.Bcast(&betaID2node,num_procs,MPI::INT, MASTER);
  myBeta=betas[node2betaID[myRank]];

  App::init(myBeta);
  GranCan * grancan = App::getGranCan();
  //grancan->initGranCan(argc, argv);
  cout<< "finished intilialising"<< endl;

  // MPI::COMM_WORLD.Barrier();

  cout<< "betas set"<< endl;
  double lastBeta= -1.0;

  for(int swaps=0;swaps<grancan->total_swaps;swaps++){
#ifdef DEBUG
    cout<< "MC process: "<<myRank<< " at beta: "<< myBeta<<" ";
    cout<< "betaID "<< node2betaID[myRank]<<endl;  
#endif
    //should check if beta is changed ... 
    bool myBetaChanged = true;
    if(lastBeta==myBeta){ myBetaChanged=false;}
    lastBeta= myBeta;

    int myEnergy = grancan->MC(myBeta,node2betaID[myRank],myBetaChanged);

    MPI::COMM_WORLD.Gather(&myEnergy,1,MPI::INT,
			   &energies,1, MPI::INT,
			   MASTER); 
    
    int myNChains = grancan->getNumFreeChains();
    MPI::COMM_WORLD.Gather(&myNChains,1,MPI::INT,
			   &nChains,1, MPI::INT,
			   MASTER);
    if(swapT){
      if(myRank==MASTER){
	//cout <<endl;
	int start =0;// swap even
	if (drand48()<0.5) start=1; //swap uneven
	for(int i=start;i<num_procs-1;i=i+2){
	  trySwap(i,i+1);
	}
      }
    }
    MPI::COMM_WORLD.Bcast(&node2betaID,num_procs,MPI::INT, MASTER);
    MPI::COMM_WORLD.Bcast(&betaID2node,num_procs,MPI::INT, MASTER);
    myBeta=betas[node2betaID[myRank]];
  }
  //double myEnergy = grancan->MC(steps,myBeta,node2betaID[myRank],true);

  grancan->getMCStats(num_procs,myRank);

  cout << "close down process "<<myRank<<endl;
  MPI::COMM_WORLD.Barrier();
  MPI::Finalize();
  exit(0);
}


double zz = log(App::eBetaMu);

bool trySwap(int betaID1, int betaID2){
  //cout <<"trial: "<<betaID1<<" "<<betaID2<<endl;
  int node1 = betaID2node[betaID1];
  int node2 = betaID2node[betaID2];
  double beta1 = betas[betaID1];
  double beta2 = betas[betaID2];

  double realBeta1=100*beta1;
  double realBeta2=100*beta2;

  double energy1 =energies[node1];
  double energy2 =energies[node2];

  int nChains1 = nChains[node1];
  int nChains2 = nChains[node2];

 
  double dE = (double) (energy1-energy2);
  double dB = (beta1)-(beta2);
  double prefactor;
  if(LAMBDA_COR){
    prefactor = pow(realBeta1/realBeta2,(3/2)*(nChains1-nChains2));
    //cout<<"beta1: "<<realBeta1<<" beta2: "<<realBeta2<<endl;
    //cout<<"nChains1: "<<nChains1<<" nChains2: "<<nChains2<<endl;
  }else{
    prefactor = 1.0;
  }

  double pAccSwap = prefactor*exp( dE*dB);
  //cout << "acc: "<<pAccSwap<<" prefactor: "<< prefactor<<endl;
  if(drand48()<pAccSwap){
#ifdef DEBUG
    cout << "SWAPPING " << beta1 <<" with energy: "<< energy1 <<" at node "<<node1;
    cout << " with " << beta2<<" with energy: "<< energy2<< " at node " <<node2<<endl;
#endif
    betaID2node[betaID1] =node2;
    betaID2node[betaID2] =node1;
    node2betaID[node1]=betaID2;
    node2betaID[node2]=betaID1;
    //transitionRates->changeBeta(Cnat,ligC);
    return true;
  }else{
    return false;
  }
}




int initBetas(double minBeta,double maxBeta){
  if(num_procs ==1){
    betas[0]= (minBeta + maxBeta)/2;
     betaID2node[0]=0;
     node2betaID[0]=0;
  }else{
    if(!App::linear_T){
      double step=(double)(maxBeta - minBeta)/(double) (num_procs-1);
      for (int i=0;i<num_procs;i++){
	betas[i]= minBeta + i*step;
	betaID2node[i]=i;
	node2betaID[i]=i;
	cout<< "node "<<i<<" beta "<<betas[i]<<" T "<<0.01/ betas[i]<<endl;
      }
    }else{
      double maxT =0.01/minBeta;
      double minT =0.01/maxBeta;
      double temperatures[MAX_PROCS];
      double step=(double)(maxT - minT)/(double) (num_procs-1);
      for (int i=0;i<num_procs;i++){
	temperatures[num_procs - i-1]= minT + i*step;
      }
      for (int i=0;i<num_procs;i++){
	betas[i]= 0.01/temperatures[i];
	betaID2node[i]=i;
	node2betaID[i]=i;
	cout<< "node "<<i<<" beta "<<betas[i]<<" T "<<temperatures[i]<<endl;
      }
    }
  }
  return 0;
}
