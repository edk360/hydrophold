#ifndef _App_Hpp_
#define _App_Hpp_

class GranCan;

#include <string>

using namespace std;

class App{
public:
  static void getOpts(int argc, char * argv[]);
  static void init(double myBeta);
  static GranCan * getGranCan(){return grancan;};
  static string fn_stats;
  static string fn_eMap ;
  //static string fn_hbond ;
  static string fn_in ;
  static string fn_native;
  static string fn_AAdistribution;

  static bool TempDepPotential;
  static bool linear_T;
  static bool get_Cext;
  static bool get_PeriodicPDB;
  static bool LAMBDA_COR;
  static bool useOldFormat;
  static bool designProgram;
  static bool AAdistribution;
  static bool checkBelowNative;
  static bool findLowestE;
  static bool writeCluster;
  static bool writeCluster5;
  static bool nativeStats;
  static bool hbondStats;
  static bool cintStats;
  static bool setAir;
  static bool DeltaDesign;
  static bool umbrellaSampling;
  static bool NewPotential;
  static bool HalfPotential;
  static bool SeparateT0;

  static ofstream moviestream;
  static bool recordMovie;
  static int moviestep;
  static int num_configs;
  static int tableStep;
  static int Q0;
  static int C0;
  static int SpringConstant_Nint;
  static int SpringConstant_Cint;

  static string fn_aa;
  static int indx_water;
  
  static double minBeta;
  static double maxBeta;

  static double eBetaMu;
  static double alpha;
  static double T0;

  static double designT;
private:
  static GranCan * grancan;
};




#endif
