#ifndef _AA_H_
#define _AA_H_

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <vector>

#define MAXAA 21

using namespace std;

class AA{
public:
  AA(string filename,int water_indx);
  int getInteraction(int a1,int a2);
  int getTempInteraction(int a1,int a2,double Beta,double alpha, double T0);
  int stringtoAA(string s);
  //char int2AA[20][4];
  static string int2aa[MAXAA]; 
  static int NUMAA;
  static int WATER;
  static int designWater;
private:
  int aaInt[MAXAA][MAXAA];
};


inline
int AA::getInteraction(int a1,int a2){
  return aaInt[a1][a2];
}

inline
int AA::getTempInteraction(int a1,int a2,double Beta, double alpha, double T0){


  if(Beta==0){
    return aaInt[a1][a2];
  }
  double T=1/Beta;
  if(((aaInt[a1][a2]>0) &&((a1==AA::WATER) || (a2==AA::WATER)) )){
	return aaInt[a1][a2]-alpha*(T-T0)*(T-T0);
  }
  else return aaInt[a1][a2];
}

//// general split function
vector<string> split(const string& str, const string& delimiters);


#endif
