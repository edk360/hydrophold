#include "AA.hpp"

#include <math.h>
#include <fstream>




int averageInt[20];

char  AA::int2aa[20][4] = {"Cys","Phe","Leu","Trp","Val","Ile","Met","His","Tyr","Ala","Gly","Pro","Asn","Thr","Ser","Arg","Gln","Asp","Lys","Glu"};

AA::AA(string filename){
  
 
  ifstream aaFile (filename.c_str());
  if (!aaFile.is_open()){
    cout << "could not open file: "<<filename.c_str()<<  endl;
    exit(1);
  }
  int i=0;
  while(!aaFile.eof()){
    char line[500];
    char aaName[4];
    float values[20];
    aaFile.getline(line,2000);
    sscanf(line,"%3s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",aaName,values,values+1,values+2,values+3,values+4,values+5,values+6,values+7,values+8,values+9,values+10,values+11,values+12,values+13,values+14,values+15,values+16,values+17,values+18,values+19);
    // needs to read from diagonal and above 	 
    for(int j=i;j<20;j++){
      aaInt[i][j]=(int) rint(100*values[j]);
      aaInt[j][i]=(int) rint(100*values[j]);
    }
    i++;
  }
#ifdef DEBUG
  for(int i=0;i<20;i++){
    cout << int2aa[i] <<"\t";
    for(int j=0;j<20;j++){
      cout<< aaInt[i][j]<<"\t";
    }
    cout<<endl;
  }
#endif
 // OPT would need reordering with other matrix input
  aaFile.close();
}




int AA::stringtoAA(string s){
  for(int i=0;i<20;i++){
    if(!s.compare(int2aa[i])){
      return i;
    }
  }
  cout << "aa not found:" <<s<<endl;
  exit(1);
  return(0);
}
