#ifndef _POS_HPP
#define _POS_HPP

#include <stdio.h>

#define XDIR 0 
#define YDIR 1 
#define ZDIR 2 

#define LX 80
#define LY 80
#define LZ 80

#include <string.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string.h>

#
using namespace std;



class Pos{
public:
  Pos(int tx, int ty, int tz );
  Pos();
  Pos operator+	(const Pos & v);
  Pos operator-	(const Pos & v);
  Pos& operator=  (const Pos & p);
  bool operator!= (const Pos &p);
  bool operator== (const Pos &p);
  void periodicBoundary();
  void printCout();
  string toString();
  const int operator[] ( const int indx )const;
  int& operator[] ( const int indx );
  // --- data structure ---
  union
  {
    struct
    {
      int x;
      int y;
      int z;
    };
    int	xyz[3];
  };
private:
  void copy(const Pos& p);
};


// Non-member functions
int getAngleUnitV(Pos p1,Pos p2);
int getAnglePositionV(Pos p1,Pos p2,Pos p3);
Pos pickNeighbour90(Pos p1,Pos p3, int randomN);
Pos pickNeighbour120(Pos p1,Pos p2,Pos p3);

inline
Pos::Pos(int tx,int ty,int tz){
  x=tx;y=ty;z=tz;
}

inline
Pos::Pos(){
  x=0;y=0;z=0;
}



inline
Pos Pos::operator+ (const Pos & v){
  return Pos(x+v.x,y+v.y,z+v.z);
}

inline
Pos Pos::operator- (const Pos & v){
  return Pos(x-v.x,y-v.y,z-v.z);
}


inline
bool Pos::operator!= (const Pos &p){
  return (x!=p.x || y!=p.y || z!=p.z);
}

inline
bool Pos::operator== (const Pos &p){
  return (x==p.x && y==p.y && z==p.z);
}



inline
const int Pos::operator[] (const int indx)const{
  return xyz[indx];
}

inline
int &  Pos::operator[] (const int indx){
  return xyz[indx];
}



inline
Pos&	Pos::operator= 	( const Pos & p )
{
	copy( p );
	return (*this);
}

inline
void Pos::copy ( const Pos & p ){
	x = p.x;
	y = p.y;
	z = p.z;
}





inline
void Pos::periodicBoundary(){
  if(x>=LX){
    x =x-LX;
  }
  if(x<0){
    x =LX + x;
  }
 if(y>=LY){
    y =y-LY;
  }
  if(y<0){
    y =LY + y;
  }
  if(z>=LZ){
    z =z-LZ;
  }
  if(z<0){
    z =LZ + z;
  }
}

#endif
