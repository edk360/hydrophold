#include "GranCan.hpp"
#include "Lattice.hpp"
#include "Chain.hpp"
#include "MolBox.hpp"
#include "EnergyMap.hpp"
#include "Stats.hpp"
#include "Cluster.hpp"
#include "ptemp.h"
#include "Design.hpp"
#include "StatsMethods.hpp"
#include "WaterStats.hpp"

#include "App.hpp"

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>

#define CORRECTION_0CHAINS
//#define DEBUG

extern Pos local[6];
//extern double designT; // Design.cpp


//double Volume = (double) (LX * LY * LZ) ; 
//double eBetaMu = -5; 
//double eBetaMu = 0.3; 

/*string fn_aa ="aa.txt";

double minBeta=0.01;
double maxBeta=0.05;

double pGlobal =0.1;
double pInsDel =0.01;

string fn_stats="stats_";
string fn_eMap ="eMap_";
string fn_in = "RWWLY10_solo.pdb";
string fn_outdir = "output/";

int steps =10000;
int total_swaps=1;

bool linear_T=false;
bool get_Cext=false;
bool get_PeriodicPDB=false;
bool LAMBDA_COR=false;
bool designProgram=false;
int indx_water=-1;

int num_configs= 10000; //(int)(steps * pInsDel);
*/
GranCan::GranCan(){
  mainLattice = new Lattice;
  mainLattice->init();
  molbox=new MolBox(mainLattice);  // needed to for Stats::setLattice

  pGlobal =0.1;
  pInsDel =0.;
  pChangeStrandCoil=0.5;
  
  steps =10000;
  total_swaps=1;
  
  Volume = (double) (LX * LY * LZ) ;
  App::eBetaMu = -0.0000005;
}

/*int  GranCan::initGranCan(int argc,char *argv[]){
  
  double minT=0,maxT=0;

  int optind=1;
  while ((optind < argc) && (argv[optind][0]=='-')) {
    string sw = argv[optind];
    //cout<<"* "<< sw <<endl;
    if (sw=="-minB") {
      optind++;
      minBeta = atof(argv[optind]);
    }else if(sw=="-maxB") {
      optind++;
      maxBeta = atof(argv[optind]);
    }else if (sw=="-minT") {
      optind++;
      minT = atof(argv[optind]);
    }else if(sw=="-maxT") {
      optind++;
      maxT = atof(argv[optind]);
    }else if(sw=="-S"){
      optind++;
      steps = atoi(argv[optind]);
    }else if(sw=="-W"){
      optind++;
      total_swaps = atoi(argv[optind]);
    }else if(sw=="-i"){
      optind++;
      fn_in.assign(argv[optind]);
    }else if (sw=="-ebmu") {
      optind++;
      eBetaMu = atof(argv[optind]);
    }else if(sw=="-aa"){
      optind++;
      cout<< argv[optind]<<endl;
      fn_aa.assign(argv[optind]);
    }else if(sw=="-lT"){
      linear_T=true;
    }else if(sw=="-noSwap"){
      swapT=false;
    }else if(sw=="-getCext"){
      get_Cext=true;
    }else if(sw=="-getPeriodicPDB"){
      get_PeriodicPDB=true;
    }else if(sw=="-lambdaCor"){
      LAMBDA_COR=true;
    }else if(sw=="-indxWater"){
      optind++;
      indx_water= atoi(argv[optind]);
    }else if(sw=="-designT"){
      designProgram=true;
      optind++;
      designT = atof(argv[optind]);
    }else{
      cout << "unknown option: "<< sw<<endl;
    }
    optind++;
  } 
  if(minT >0){maxBeta= 0.01/minT;cout<<"minBeta "<<maxBeta<<endl;}
  if(maxT >0){minBeta= 0.01/maxT;cout<<"maxBeta "<<minBeta<<endl;}
  if(designProgram){
    Design * des= new Design();
    cout<<"a"<<endl;
    des->init(fn_in);
    cout<<"b"<<endl;
    des->designProcedure((minBeta + maxBeta)/2.0, steps);
    exit(0);
  }

  num_configs= (int)(steps * pInsDel * 1.1);
  if(num_configs < 10) num_configs=10;
  if(num_configs > MAX_CONFIGS)num_configs =MAX_CONFIGS;
  cout<< "num_configs per set: "<<  num_configs<< endl;
  cout<<"eBetaMu "<<eBetaMu<<endl;
  if(eBetaMu<0){pInsDel=0.0;}
  molbox->setAA(fn_aa,indx_water);
  mainLattice->setAA(fn_aa,indx_water);

  mainLattice->readPDBMultiChain(fn_in);
  cout<< "read file "<<fn_in<<endl;
  if(eBetaMu>0){
    molbox->setMolecule(mainLattice->chains[0]);
    cout<< "molbox molecule set "<<endl;
  }

  if(get_Cext){
    getCext();
    exit(0);
  }
  if(get_PeriodicPDB){
    mainLattice->printPeriodicPDB();
    exit(0);
  }
  return(0);
}*/


int GranCan::getCext(){
  mainLattice->stats.printCout();
  cout<<"CEXT "<<App::fn_in<<" "<<mainLattice->stats.getCext()<<endl;
  return mainLattice->stats.getCext();
}


int GranCan::MC(double beta, int betaId, bool betaChanged){
  totalEmptySteps=0;
#ifdef DEBUG
  cout<< "starting MC "<<endl;
#endif
 
  mainLattice->setBetaMoves(beta);
  //cout<< "beta mainLattice set "<<endl;
  if(App::eBetaMu>0){ 
    molbox->createNewSet(beta,App::num_configs,betaChanged);
  }
#ifdef DEBUG
  cout<< "new config  set created"<<endl;
#endif
  mainLattice->energyMap->setBetaID(betaId);


  /// check if statistics match, calculated statistics
  mainLattice->checkStats();
  mainLattice->waterStats->checkWaterStats();

  for(long i=0;i<steps;i++){
#ifdef DEBUG    
    cout<< i <<endl;  mainLattice->stats.printCout();
#endif
    if(drand48() < pGlobal){
      Chain * c= mainLattice->chains[(int) floor(mainLattice->nChains * drand48())];
      if(!c->locked){
	c->globalMove();
      }
    }
    if(drand48() < pInsDel){
      //molbox->printConfig(0);
      
      if(drand48()<0.5){
		trialInsertChain();
      }else{
		trialDeleteChain();
      }
    }
    Chain * c= mainLattice->chains[(int) floor(mainLattice->nChains * drand48())];
    if(!c->frozen){
      c->localMove();
    }
    //mainLattice->checkStats();
    
  }

  ClusterInfo ci;
  if(pInsDel >0){
    Cluster cl;
    ci = cl.checkCluster(mainLattice,beta);
  }
  
  /// write stats to table, with weigh of one
  mainLattice->energyMap->writeTableStats(mainLattice->stats,App::tableStep,1.0,ci,mainLattice->waterStats);
  /// empty Steps, record stats for simulating empty box (Gran Canonical only)
  if(totalEmptySteps>0){
    Stats emptyStats;
    double weight = totalEmptySteps/(double)steps;
    ClusterInfo ci_empty = {0,0,0};
    mainLattice->energyMap->writeTableStats(emptyStats,- App::tableStep,weight,ci_empty,mainLattice->waterStats);
  }
  App::tableStep++;

  /// check if statistics match, calculated statistics
  mainLattice->checkStats();
  mainLattice->waterStats->checkWaterStats();

  return mainLattice->stats.getEtot(); 
}




int GranCan::trialInsertChain(){
#ifdef DEBUG 
  cout<< "trial insert chain"<<endl; 
#endif
  // find free chains
  int l=0;
  for(int c=0;c<mainLattice->nChains;c++){
    if(! mainLattice->chains[c]->hasCext() ){
      l++;
    }
  }  
  int freeChains=l;
 // accept depending on mu
  double prefactor;
  if(LAMBDA_COR){
    prefactor = 1.0/pow(mainLattice->getRealBeta(),3.0/2.0);
  }else{
    prefactor =1.0;
  }


  double accRatio = prefactor* App::eBetaMu * ((double) Volume/ ((double)freeChains +1.0));
  if(drand48() < accRatio){

    // get configuration
    Config * config =  molbox->getNewConfig();
    Config * newConfig= new Config;
    newConfig->nTotal = config->nTotal;
    // add random translation to points (could do this in molbox) 
    Pos addP;
    addP.x = (int) floor( LX * drand48());
    addP.y = (int) floor( LY * drand48());
    addP.z = (int) floor( LZ * drand48()); 
    
    for(int n=0;n<config->nTotal;n++){
      //cout<<"config res:"<<n<<" "<<config->x[n]<<" "<<config->y[n]<<" "<<config->z[n]<<endl;
      Pos posN = config->positions[n]+addP;
      posN.periodicBoundary();
      // check if lattice points not occupied and not bound
      if(mainLattice->getResidue(posN) != NULL){
	delete newConfig;
	return -1;
      }
      for(int k=0;k<6;k++){
	Pos posNB = local[k] + posN;
	posNB.periodicBoundary();
	// check if bound, threonine (aa==13) contact does not count
	if(mainLattice->getResidue(posNB)!= NULL ) {
	  delete newConfig;
	  return -1;
	}
      }
      newConfig->positions[n]= posN;
    }
    
    
    // insert, by creating new chain
    mainLattice->insertChain(newConfig,molbox->sequence,mainLattice->getBetaMoves());
    delete newConfig;
    // mainLattice->writePDBMultiChain("newlyinsertedchain.pdb");
    return 1;
  }
  return 0;
}

// this may become slow for fully aggregated box
int GranCan::trialDeleteChain(){
  // cout<< "trial delete chain"<<endl;
  int freeChns[MAX_CHAINS];
  // find free chains
  int l=0;
  for(int c=0;c< mainLattice->nChains;c++){
    if(! mainLattice->chains[c]->hasCext()){
      freeChns[l]= c;
      l++;
    }
  }  
  int freeChains=l;
  if(freeChains < 1) return -2;
  if(mainLattice->nChains <=1 ){ 
#ifdef CORRECTION_0CHAINS
    double prefactor;
    if(LAMBDA_COR){
      prefactor = pow(mainLattice->getRealBeta(),3.0/2.0);
    }else{
      prefactor =1.0;
    }
 

    double accRatio = prefactor*((double)freeChains ) / (App::eBetaMu *  Volume);
    if(drand48() < accRatio){
      double alpha = App::eBetaMu * ((double) Volume) / prefactor;
      double num_steps;
      if(alpha > 0.9999){
        num_steps=1;
      }else{
        num_steps = ceil(log(drand48())/log(1.0-alpha));
      }
      double moves = (1.0+ pGlobal)*(2*num_steps/pInsDel);
      totalEmptySteps += moves;
      Stats emptyStats;
      mainLattice->energyMap->mapStats(emptyStats,moves, 0);
    }
    return -1;
#else
    return -1;
#endif
  }
  /*do{  
    chain = (int) floor(drand48() * nChains);  
    }while(chains[chain]->Cext!=0);
  */
	
  // accept deletion depending on mu


  double prefactor;
  if(LAMBDA_COR){
    prefactor = pow(mainLattice->getRealBeta(),3.0/2.0);
  }else{
    prefactor =1.0;
  }
  double accRatio = prefactor*((double)freeChains ) / (App::eBetaMu *  Volume);
  if(drand48() < accRatio){
     // pick a free chain at random
   
    int chain= freeChns[(int) floor(drand48() * freeChains)];
    // delete chain from lattice
    mainLattice->deleteChain(chain);
    return 1;
  }
  return 0;
}

int GranCan::getNumFreeChains(){
  // cout<<"getNumfree chains" <<endl;
 int l=0;
  for(int c=0;c< mainLattice->nChains;c++){
    if(! mainLattice->chains[c]->hasCext()){
      l++;
    }
  } 
  return l;
}



void GranCan::getMCStats(int num_procs, int myRank){
  std::stringstream outPDB;
  std::stringstream outPDBNative;
  outPDB  << "outN"<<myRank<<".pdb";
  outPDBNative << "outN_Native" << myRank << ".pdb";
  mainLattice->writePDBMultiChain(outPDB.str());
  mainLattice->writePDBMultiChainNatives(outPDBNative.str());


  std::stringstream outSTATS;
  outSTATS  << App::fn_stats << "N"<<myRank <<".tab"; 
  mainLattice->energyMap->printGnuPlotDataWR(outSTATS.str(),num_procs,myRank);
  
  
  std::stringstream outEMAP;
  outEMAP << App::fn_eMap << "N"<<myRank <<".tab"; 
  mainLattice->energyMap->printEMap(outEMAP.str(),num_procs,myRank);

  std::stringstream outCLSIZE;
  outCLSIZE << "clusterSize" << "N"<<myRank <<".tab"; 
  mainLattice->energyMap->printClusterSize(outCLSIZE.str(),num_procs,myRank);



  mainLattice->stats.printCout();
  cout<<"free chains: "<<getNumFreeChains()<<endl;

  mainLattice->waterStats->printWstats();


  Cluster cl;
  cl.startCounting(mainLattice);
  cl.printCout();
    
  //printGnuPlotData(fn_stats,num_procs,myRank);
  //printEMap(fn_eMap,num_procs,myRank);
  //transitionRates->print2file("transitions",myRank);
  //sqrg->print2file("sqrg",myRank);
}
