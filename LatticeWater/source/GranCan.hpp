#ifndef _GranCan_H_
#define _GranCan_H_

#include <string.h>
#include <iostream>




using namespace std;


class MolBox;
class Lattice;


class GranCan{
public:
  GranCan();
  int initGranCan(int argc,char *argv[]);
  int MC(double beta, int betaId, bool betaChanged);
  int trialInsertChain();
  int trialDeleteChain();
  int getNumFreeChains();
  void getMCStats(int num_procs, int myRank);

  double pGlobal;
  double pInsDel;
  double pChangeStrandCoil;
  
  int steps;
  int total_swaps;
  double totalEmptySteps;
  double Volume;


  //private:
  int getCext();
  MolBox * molbox;
  Lattice * mainLattice;
};

/*extern double eBetaMu;
extern double minBeta;
extern double maxBeta;
extern int total_swaps;
extern string fn_aa;
extern bool linear_T;
*/

#endif
