#include "Native.hpp"

#ifdef NATIVE
Native::Native(Lattice * l){
  int totalC=0;
  for(int k=0;k<MAX_CHAINS;k++){
    for(int l=0;l<MAX_CHAINS;l++){
      for(int i=0;i<MAX_RES;i++){
	for(int j=0;j<MAX_RES;j++){ 
	  contacts[k][l][i][j]=0;
       }
      }
    } 
  }
  
  for(int nc=0;nc<l->nChains;nc++){
    for(int n=0;n<l->chains[nc]->N;n++){
     Residue * res = l->chains[nc]->residues[n];
     for(int k=0;k<6;k++){
       int nx = res->x +  local[k][0];    
       int ny = res->y +  local[k][1];
       int nz = res->z +  local[k][2];
       if(l->r[nx][ny][nz]!=NULL){
	 Residue * myNeighbour = l->r[nx][ny][nz];
	 if(myNeighbour->chainNum !=  res->chainNum ||abs(myNeighbour->n - res->n) !=1){
	   contacts[res->chainNum][myNeighbour->chainNum][res->n][myNeighbour->n]=1;
	   totalC++;
	 }
       }
     }
   }
 }
  
  totalC =totalC/ 2;
  totCnat=totalC;
}

#endif
