#ifndef _Native_H_
#define _Native_H_


#include "Lattice.hpp"
#include "Chain.hpp"
#include "Stats.hpp"




class Native{
public:
  Native(Lattice *l);
  void setNative(Lattice *l);
  bool isNativeContact(int chainA, int chainB, int resA,  int resB);
  bool isBelowNativeEnergy(int Etot);
  int getNativeEnergy();
  bool hasNativeStructure(int Ntot);
private:
  bool ****contacts;
  int totCnat;
  int nativeEnergy;
};



inline
bool Native::isNativeContact(int chainA, int chainB,int resA, int resB){
  return contacts[chainA][chainB][resA][resB];
}

inline
bool Native::isBelowNativeEnergy(int Etot){
  return (Etot < nativeEnergy);
}

inline
bool Native::hasNativeStructure(int Ntot){
  return (Ntot == totCnat);
}


inline
int Native::getNativeEnergy(){
  return nativeEnergy;
}


#endif
