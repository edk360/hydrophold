#include "Cluster.hpp"
#include "Lattice.hpp"
#include "Residue.hpp"
#include "Chain.hpp"
#include "AA.hpp"
#include "Stats.hpp"
#include "EnergyMap.hpp"
#include "App.hpp"

#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>


Cluster::Cluster(){
}


int Cluster::oldClusterSize=0;

void Cluster::reset(){
  largestCluster=0;
  totalClusters=0;
  totalVisited=0;
  for(int i=0 ;i<MAX_CHAINS;i++){
    visited[i] = -1;
  }
  for(int i=0 ;i<MAX_CLUST;i++){
    clusterCount[i] = 0;
  }
}


void Cluster::startCounting(Lattice * l1){
  l=l1;
  reset();
  int curCluster=0;
  // cout<<"starting cluster count!!"<<endl;
  for(int i=l->nChains -1;i>=0;i--){
    if(visited[i]<0){
      visit(i,curCluster);
      if(clusterCount[curCluster]>clusterCount[largestCluster]){
	largestCluster=curCluster;
      }
      curCluster++;
    }
  }
  totalClusters = curCluster;
}


void  Cluster::visit(int chain, int cluster){
  clusterCount[cluster]++;
  totalVisited++;
  visited[chain]=cluster;
  for(int n=0; n< l->chains[chain]->N;n++){
    if(totalVisited >= l->nChains){
      //cout<<"early return"<<endl;
      return;
    }
    Residue * res= l->chains[chain]->residues[n];
    //if(res->aa !=13){
      for(int k=0;k<6;k++){

	Pos nb =local[k]+ res->pos;
	nb.periodicBoundary();
	Residue * neighbour =l->getResidue(nb); 
	if(neighbour!=NULL && neighbour->aa != 13){
	  int nChain = neighbour->chainNum;
	  if(visited[nChain]<0){
	    visit(nChain,cluster);
	  }
	}
	// }
    }
  }
}



void Cluster::printCout(){
  for(int c=0;c<totalClusters;c++){
    cout <<"cluster "<<c<< " contains ";
    cout << clusterCount[c] << " chains"<<endl;
  }
}


ClusterInfo Cluster::checkCluster(Lattice *l1,double beta){
  l=l1;
  startCounting(l);
  l->energyMap->recordClusterSize(clusterCount[largestCluster]);
  ClusterInfo ci;
  
  int lowLim = (int)(0.95* (double)oldClusterSize);
  int highLim = (int)(1.05* (double)oldClusterSize);
  if(App::writeCluster && 
     (clusterCount[largestCluster] < lowLim || clusterCount[largestCluster]> highLim )){
  
    writePDBClust(largestCluster,beta);
    
    oldClusterSize= clusterCount[largestCluster];

  }
 
  if(App::writeCluster5 ){
    int sizeMaxClust = clusterCount[largestCluster];  
    if(( sizeMaxClust != oldClusterSize) && 
       (sizeMaxClust==5 || sizeMaxClust==10 || sizeMaxClust==15 || 
	sizeMaxClust==20 || sizeMaxClust==50|| sizeMaxClust==100 || 
	sizeMaxClust==150 || sizeMaxClust==200 )){
     
      writePDBClust(largestCluster,beta);
      oldClusterSize=sizeMaxClust;
    }
  }

  //printCout();
  ci.clusters=totalClusters;
  ci.largestCluster = clusterCount[largestCluster];
  ci.totalChains = l->nChains;

  return ci;
}

/*int Cluster::checkCluster(Lattice *l1,double beta, int required_cluster_size){
  l=l1;
  startCounting(l);
  if(clusterCount[largestCluster]==required_cluster_size){
    cout <<"Largest cluster "<< largestCluster << " size "<<clusterCount[largestCluster]<<endl; 
    writePDBClust(largestCluster,beta);
    return 1;
  }
  printCout();
  return 0;
  }*/

/*int checkClusteAltr(Lattice *l,int myRank, int required_cluster_size){
  startCounting(l);
  for(int c=0;c<totalClusters;c++){
    if(clusterCount[c]==required_cluster_size){
      std::stringstream outPDB;
      outPDB << "outN"<<myRank<<"cl"<<clusterCount[c]<<".pdb";
      l->writePDBMultiChain(outPDB.str());
      std::stringstream outClust;
      outClust << "clustN"<<myRank<<"cl"<<clusterCount[c]<<".out";
      ofstream outfile(outClust.str().c_str());
      for(int cc=0;cc<totalClusters;cc++){
	outfile <<"cluster "<<cc<< " contains ";
	outfile << clusterCount[cc] << " chains"<<endl;
      }
      outfile.close();
      return 1;
    }
  }
  printCout();
  return 0;
  }*/



void Cluster::writePDBClust(int clust, double beta){
  std::stringstream buf;
  buf << std::fixed << std::setprecision(2) << 0.01/beta; 
  string stT= buf.str();
  int size=clusterCount[clust]; 

  std::stringstream outPDB;
  outPDB << "clusT"<<stT<<"S"<<size<<"all.pdb";
  l->writePDBMultiChain(outPDB.str());

  std::stringstream outPDBclust;
  outPDBclust << "clusT"<<stT<<"S"<<size<<".pdb";
  ofstream outs;
  cout<< "printing to "<< outPDBclust.str()<<endl;
  outs.open(outPDBclust.str().c_str()); 
  outs << "MODEL "<<endl;
  //outs <<  setw(7) << moviestep<<endl;
  for(int nc=0;nc<l->nChains;nc++){
    if(visited[nc]==clust){
      Chain * ch = l->chains[nc];
      if(ch->frozen && ch->locked){
	outs << "LIG"<<endl ;
      }else if(ch->frozen){
	outs << "RIG"<<endl;
      }
      for (int n=0;n<ch->N;n++){
	Residue * res =ch->residues[n];
	outs << "ATOM  " ;
	outs << setw(5)<< n;
	outs << "  CA  ";
	outs << setw(3)<< AA::int2aa[res->aa];
	outs << " "<< (char)('A' +(res->chainNum)%('A' - 'z'));    // " A";
	outs << setw(4)<<n;
	outs << "   ";
	outs << setw(8)<< 3.0*(float) res->pos.x ;
	outs << setw(8)<< 3.0*(float) res->pos.y ;
	outs << setw(8)<< 3.0*(float) res->pos.z ;
	outs << "   1.00 22.00";
	outs << endl;
      }
      outs <<"TER   " << endl; 
    }
  }
  outs.close();

  
  std::stringstream outClust;
  outClust << "clusT"<<stT<<"S"<<size<<".out";
  ofstream outfile(outClust.str().c_str());
  outfile << "beta: "<<beta<<endl;
  outfile << "beta: "<<stT<<endl;
  outfile << "eBetaMu: "<<App::eBetaMu<<endl;
  outfile << "LX LY LZ: "<<LX<<" "<<LY<<" "<<LZ<<endl;
  //print stats
  outfile << l->stats.print2string()<<endl;
  // print all stats
  for(int cc=0;cc<totalClusters;cc++){
    outfile <<"cluster "<<cc<< " contains ";
    outfile << clusterCount[cc] << " chains"<<endl;
  }
  outfile.close();
}
