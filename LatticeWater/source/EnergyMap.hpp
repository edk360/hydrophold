#ifndef _EnergyMap_H_
#define _EnergyMap_H_

#include <string.h>
//#include <ext/hash_map>
#include <tr1/unordered_map>
#include <fstream>

#include "ptemp.h"


#define QMAX 50000
#define AGGR

using namespace std;

class Stats;
class Lattice;
class WaterStats;

struct ClusterInfo{
  int clusters;
  int largestCluster;
  int totalChains;
};




class EnergyMap{
public:
  EnergyMap(Lattice * l);
  void setBetaID(int bID);
  void printGnuPlotDataWR(string fn_stats,int num_procs, int myRank);
  void printEMap(string fn_stats,int num_procs, int myRank);
  void mapStats(Stats &s, double boltz, int totalE);
  void recordClusterSize(int sizeLargestCluster);
  void printClusterSize(string fn,int num_procs, int myRank);

  void initTableStats();
  void writeTableStats(Stats & stats, int step,double weight, ClusterInfo ci,WaterStats * ws);
  void finalizeTableStats();
  void checkBelowNative(Stats & stats, double boltz, int totalE);
private:
#ifdef AGGR
  double energyMap[MAX_PROCS][QMAX];
  int clusterSize[MAX_PROCS][QMAX];
#else
  // __gnu_cxx::hash_map<int,double> energyMap[MAX_PROCS][QMAX];
  tr1::unordered_map<int,double> energyMap[MAX_PROCS][QMAX];
#endif
  //__gnu_cxx::hash_map<int,double> eMap[MAX_PROCS];
  tr1::unordered_map<int,double> eMap[MAX_PROCS];

  /*int maxE;
  int minE;  
  int Ei;
  int Eb;*/

  int betaID;
  Lattice *l;
  ofstream tableStatsStream;
};




#endif
