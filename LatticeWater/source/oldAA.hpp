#ifndef _AA_H_
#define _AA_H_

#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

class AA{
public:
  AA(string filename);
  int getInteraction(int a1,int a2);
  int stringtoAA(string s);
  //char int2AA[20][4];
 static char int2aa[20][4]; 
private:
  int aaInt[20][20];
};


inline
int AA::getInteraction(int a1,int a2){
  return aaInt[a1][a2];
}

#endif
