#ifndef _Stats_H_
#define _Stats_H_

#include "Residue.hpp"

#include "EnergyMap.hpp"


#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string.h>

#define AGGR



using namespace std;

class Lattice;
class Chain;


class Stats{
public:
  //inline StatsMethods.hpp
  Stats();  
  void clean();
  Stats operator+(const Stats & s) const;
  Stats & operator=(const Stats & s);
  Stats & operator+= (const Stats& s);
  Stats & operator-= (const Stats& s);
  const bool  operator!= (const Stats& s) const;
  int getCext(){return Cext;};
  int getEtot(){return Etot;};
  int getNint(){return Nint;};
  int getCint(){return Cint;}

  Stats  delta(const Stats & Snew, const Stats & Sold)const;
  int getDeltaE(const Stats & old)const;  

  void localStats(Residue * res,const Pos & pos, Lattice * l);
  void localStatsAA(Residue * res,const Pos & pos, int aa, Lattice * l);
  void localStatsExclude(Residue * res,const Pos & pos, Lattice * l,int start,int end);
  void solventStats(Pos & pos,Lattice * l);
  //void localStats(Residue * res,const Pos & pos,const  Pos & spin, Lattice * l);
  //void localStatsExclude(Residue * res,const Pos & pos,const  Pos & spin, Lattice * l,int start,int end);

  //Stats.cpp
  void getLatticeStats(Lattice * l);
  void get_Eint_Cint(Chain*  c,Lattice *l);
  void printCout();
  string print2string();
  
  //static void setLattice(Lattice * l);

 
 

private:
  friend class EnergyMap;

  int Etot;
  int Ctot;  
  int Cext;
  int Eint;
  int Cint;
  int Eext;
  int Esol;
  //friend void EnergyMap::mapStats(Stats &s, double boltz);
  
  
  //int Ctot;  

  int Nint;
  int Next;
  int Ntot;

  //static Lattice * l;
};

/*** NON-MEMBER FUNCTIONS ***/





#endif
