#include <math.h>

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <tr1/unordered_map> 

#include "EnergyMap.hpp"
#include "StatsMethods.hpp"
#include "Stats.hpp"
#include "Lattice.hpp"
#include "Native.hpp"
#include "App.hpp"
#include "WaterStats.hpp"

//#define AGGR

EnergyMap::EnergyMap(Lattice * l1){
  /* maxE=0;
  minE=0;  
  Ei=0;
  Eb=0;*/

  l=l1;

#ifdef AGGR
  for(int i=0;i<MAX_PROCS;i++){
    for(int j=0;j<QMAX;j++){
      clusterSize[i][j]=0;
    }
  }
#endif
}

void EnergyMap::setBetaID(int bID){
  betaID=bID;
}

void EnergyMap::mapStats(Stats & stats, double boltz,  int totalE){
  if(!App::nativeStats){
    if(stats.Cext < 0 || stats.Cext >=QMAX){
      cout<<"ERROR too many (" << stats.Cext << ") external contacts"<<endl;
      exit(1);
    }
    if(isnan(boltz)){
      cout<<"ERROR boltz nan"<<endl;
      cout<< "totalE "<<totalE<<endl;
      cout<< "process "<<myRank<<endl;
      stats.printCout();
      cout<<"boltz "<<boltz<<endl;
      exit(1);
    }
    energyMap[betaID][stats.Cext]+= boltz;
  }else{
    /// check if within limits
    if(stats.Nint < 0 || stats.Nint >=QMAX){
      cout<<"ERROR too many (" << stats.Nint << ") native contacts"<<endl;
      exit(1);
    }
    /// check if we need to find a lowest energy structure
    if(App::checkBelowNative && l->native->isBelowNativeEnergy(totalE)){
      checkBelowNative(stats, boltz, totalE);
    }
    /// map stats, depending on which type is requested
    if(App::cintStats){
      energyMap[betaID][stats.Cint] += boltz;
    }else{
      energyMap[betaID][stats.Nint] += boltz;
    }
  }

  
  ///add energy (for calculation CV)
  eMap[betaID][totalE] +=boltz;

}

/// gives problem with multiple processes
int lowestEfound=8000;
void EnergyMap::checkBelowNative(Stats & stats, double boltz,int totalE){
  if(App::findLowestE){
    if( lowestEfound > totalE){
      cout<< "energy below native energy"<<endl;
      cout<< "Mapped stats:"<<endl;
      stats.printCout();
      cout<<"totalE "<<totalE<<endl; 
     cout<< "Lattice stats:"<<endl;
      l->stats.printCout();
      l->waterStats->printWstats();
      if(l->waterStats->totalE == totalE){
	std::stringstream outPDB;
	outPDB << "lowestEnergyN"<<myRank <<".pdb";
	l->writePDBMultiChain(outPDB.str());
	lowestEfound=totalE;
	std::stringstream outSTATS;
	outSTATS  << "lowestEnergyN"<<myRank <<".txt";
	ofstream infoFile;
	infoFile.open(outSTATS.str().c_str());
	infoFile<<stats.print2string()<<endl;
	infoFile<<"totalE "<<totalE<<endl;
	infoFile.close();
      }
    }
  }else{
    if(! l->native->hasNativeStructure(stats.Ntot) ){
      cout<<"ERROR energy below native energy"<<endl;
      cout<< "Mapped stats:"<<endl;
      stats.printCout();
      cout<<"totalE "<<totalE<<endl;
      cout<< "Lattice stats:"<<endl;
      l->stats.printCout();
      l->waterStats->printWstats();
      if(l->waterStats->totalE == totalE){
	l->writePDBMultiChain("belowNativeEnergy.pdb");
	exit(1);
      }
    }
  }
}

void EnergyMap::recordClusterSize(int sizeLargestCluster){
  clusterSize[betaID][sizeLargestCluster]++;
}



void EnergyMap::printGnuPlotDataWR(string fn_stats,int num_procs, int myRank){
  ofstream energyFile;
  energyFile.open(fn_stats.c_str());
  cout << "printing to "<<fn_stats <<endl;

  for(int betaID=0;betaID<num_procs;betaID++){ 
    energyFile<<"BETA\t"<<100.0 *  betas[betaID] << endl;
    
    for(int q=0;q<QMAX;q++){
#ifdef AGGR
      if(energyMap[betaID][q]>0){
	energyFile<< q <<"\t"<< energyMap[betaID][q] <<endl;
      }
#else
      //__gnu_cxx::hash_map<int,double>::iterator iter;   
      tr1::unordered_map<int,double>::iterator iter;
      for( iter = energyMap[betaID][q].begin(); iter != energyMap[betaID][q].end(); iter++ ) {
	energyFile<< q <<"\t"<< (double) iter->first <<"\t"<< iter->second<<endl;
      }
#endif
    }
    //energyFile.close();
  }
  energyFile.close();
  cout << "finished printing to "<<fn_stats <<endl;
}



void EnergyMap::printEMap(string fn_stats,int num_procs, int myRank){
  
  ofstream energyFile;
  energyFile.open(fn_stats.c_str());
  cout << "printing to "<<fn_stats <<endl;
  for(int betaID=0;betaID<num_procs;betaID++){
    energyFile<<"BETA\t"<<100.0 *  betas[betaID] << endl;
    //__gnu_cxx::hash_map<int,double>::iterator iter;
    tr1::unordered_map<int,double>::iterator iter;
    for( iter = eMap[betaID].begin(); iter != eMap[betaID].end(); iter++ ) {
      energyFile<< (double) iter->first / 100.0 <<"\t"<< iter->second<<endl;
    }
  }
  energyFile.close();
}


void EnergyMap::printClusterSize(string fn,int num_procs, int myRank){
  
  ofstream clusterFile;
  clusterFile.open(fn.c_str());
  cout << "printing to "<<fn <<endl;
  for(int betaID=0;betaID<num_procs;betaID++){
    clusterFile<<"BETA\t"<<100.0 *  betas[betaID] << endl;
    //__gnu_cxx::hash_map<int,double>::iterator iter;
    tr1::unordered_map<int,double>::iterator iter;
    for(int i=0;i<QMAX;i++){
      if(clusterSize[betaID][i]>0){    
	clusterFile<< i <<"\t"<< clusterSize[betaID][i]<<endl;
      }
    }
  }
  clusterFile.close();
}


void EnergyMap::initTableStats(){
  std::stringstream fnStats;
  fnStats  << "statsTable"<<myRank <<".txt";
  tableStatsStream.open(fnStats.str().c_str());

  tableStatsStream<<"step"<<"\t";
  tableStatsStream<<"T"<<"\t";

  tableStatsStream<<"beta"<<"\t";
  // tableStatsStream <<"boltz"<<"\t";

  tableStatsStream<<"weight"<<"\t";

  tableStatsStream <<"Eint"<<"\t";
  tableStatsStream <<"Eext"<<"\t";
  tableStatsStream <<"Esol"<<"\t";
  tableStatsStream <<"Etot"<<"\t";

  tableStatsStream <<"Cint"<<"\t";
  tableStatsStream <<"Cext"<<"\t";
  tableStatsStream <<"Ctot"<<"\t";
  tableStatsStream <<"solvHN"<<"\t";
  tableStatsStream << "solvN" << "\t";

 if(App::nativeStats){
    tableStatsStream <<"Nint"<<"\t";
    tableStatsStream <<"Next"<<"\t";
    tableStatsStream <<"Ntot"<<"\t";
  }
 tableStatsStream <<"wcHN"<<"\t";
 tableStatsStream << "Corr_E"<<"\t";
 tableStatsStream << "E0" << "\t";

 //  if(App::hbondStats){
 // tableStatsStream <<"Hint"<<"\t";
 // tableStatsStream <<"Hext"<<"\t";
 // tableStatsStream <<"Htot"<<"\t";
 // }
 tableStatsStream << "fsolvHN" << "\t";
 tableStatsStream << "surfHN" << "\t";
  if(App::eBetaMu>0){
    tableStatsStream <<"eBM"<<"\t";
    tableStatsStream <<"clust"<<"\t";
    tableStatsStream <<"maxCl"<<"\t";
    tableStatsStream <<"nChains"<<"\t";
  }

  tableStatsStream <<endl;
}

void EnergyMap::writeTableStats(Stats & stats,int step,double weight, ClusterInfo ci, WaterStats * ws){
  double beta = betas[betaID]*100;
  std::stringstream buf;
  buf << std::fixed << std::setprecision(2) << 1/beta;

  tableStatsStream<<step<<"\t";
  tableStatsStream<<buf.str()<<"\t";

  tableStatsStream <<beta <<"\t";
  //tableStatsStream <<boltz<<"\t";

  tableStatsStream <<weight <<"\t";

  tableStatsStream <<stats.Eint<<"\t";
  tableStatsStream <<stats.Eext<<"\t";
  tableStatsStream <<ws->stats.solvE<<"\t";
  tableStatsStream <<ws->totalE<<"\t";

  tableStatsStream <<stats.Cint<<"\t";
  tableStatsStream <<stats.Cext<<"\t";
  tableStatsStream <<stats.Ctot<<"\t";
  tableStatsStream <<ws->stats.solvHN << "\t";
  tableStatsStream << ws -> stats.solvN<< "\t";

  if(App::nativeStats){
    tableStatsStream <<stats.Nint<<"\t";
    tableStatsStream <<stats.Next<<"\t";
    tableStatsStream <<stats.Ntot<<"\t";
  }

  //if(App::hbondStats){
  //tableStatsStream <<stats.Hint<<"\t";
  // tableStatsStream <<stats.Hext<<"\t";
  // tableStatsStream <<stats.Htot<<"\t";
  //}
 // tableStatsStream << stats.Etot + 1000.*(ws->stats.solvHN)*(0.50-(1/beta)*(0.50-(1/beta))) <<"\t";
  tableStatsStream << ws->stats.wcHN << "\t";
  if(!App::TempDepPotential){
    tableStatsStream << ws->totalE << "\t" ;
  }
  else if(App::NewPotential){
    tableStatsStream << ws->totalE  + App::alpha * ws->stats.wcHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.wcHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
  }
  else if(App::HalfPotential && App::SeparateT0){
    //tableStatsStream << ws->totalE + App::alpha * ws->stats.solvHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.solvHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
    tableStatsStream << ws->totalE + SURALPHA * SURWEIGHT * ws->stats.surfaceHN *(1/beta-SURFACET0)*(1/beta-SURFACET0) - SURALPHA*SURWEIGHT*ws->stats.surfaceHN*(SURFACET0*SURFACET0-(1/beta)*(1/beta))
    		                       + SOLALPHA * ws->stats.solvatedHN *(1/beta-SOLVATET0)*(1/beta-SOLVATET0) - SOLALPHA*ws->stats.solvatedHN*(SOLVATET0*SOLVATET0-(1/beta)*(1/beta))<<"\t";
  }
  else if(App::HalfPotential){
	    tableStatsStream << ws->totalE + App::alpha * ws->stats.surfaceHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.surfaceHN)*(App::T0*App::T0-(1/beta)*(1/beta))
	    		                       + App::alpha * ws->stats.solvatedHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.solvatedHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
  }
  else{
	tableStatsStream << ws->totalE + App::alpha * ws->stats.solvHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.solvHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
  }

  if(!App::TempDepPotential){
    tableStatsStream << ws->totalE << "\t";
  }
  else if(App::NewPotential){
    tableStatsStream << ws->totalE  + App::alpha * ws->stats.wcHN *(1/beta-App::T0)*(1/beta-App::T0) << "\t";
  }
  else if(App::HalfPotential && App::SeparateT0){
    tableStatsStream << ws->totalE + SURALPHA * SURWEIGHT * ws->stats.surfaceHN *(1/beta-SURFACET0)*(1/beta-SURFACET0)  + SOLALPHA * ws->stats.solvatedHN *(1/beta-SOLVATET0)*(1/beta-SOLVATET0) << "\t" ;
  }
  else if(App::HalfPotential){
    tableStatsStream << ws->totalE + App::alpha * ws->stats.surfaceHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.surfaceHN)*(App::T0*App::T0-(1/beta)*(1/beta))
   		                       + App::alpha * ws->stats.solvatedHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.solvatedHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
  }
  else{
    tableStatsStream << ws->totalE + App::alpha * ws->stats.solvHN *(1/beta-App::T0)*(1/beta-App::T0) - App::alpha*(ws->stats.solvHN)*(App::T0*App::T0-(1/beta)*(1/beta))<<"\t";
  }
  tableStatsStream << ws->stats.solvatedHN << "\t";
  tableStatsStream << ws->stats.surfaceHN;


  if(App::eBetaMu>0){
    tableStatsStream <<App::eBetaMu<<"\t";
    tableStatsStream <<ci.clusters<<"\t";
    tableStatsStream <<ci.largestCluster<<"\t";
    tableStatsStream <<ci.totalChains<<"\t";
  }

  tableStatsStream <<endl;
}


void EnergyMap::finalizeTableStats(){
  tableStatsStream.close();
}
