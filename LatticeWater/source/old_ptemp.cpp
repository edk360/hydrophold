#include <stdio.h>
#include <mpi.h>
#include <iostream>
#include <math.h>

#include "MC.hpp"
#include "ptemp.h"


using namespace std;
//using namespace __gnu_cxx;

//mpiCC tstmpi.cpp -o tstmpi



// MESSAGE TYPES
#define SWAP_REQUEST 1
#define ALL_DONE 2



// FORWARD DECLARATIONS

bool trySwap(int,int);
int initBetas(double minBeta,double maxBeta);

// GLOBALS
int MASTER =0;
int myRank;


double betas[MAX_PROCS];
int betaID2node[MAX_PROCS];
int node2betaID[MAX_PROCS];
int energies[MAX_PROCS];



double myBeta;
int num_procs;

//Rates * transitionRates;

//double fraction_swaps=0.01;


int main(int argc, char *argv[]) {

  int steps=0;
  char name[MPI_MAX_PROCESSOR_NAME];
  int namelen;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  MPI::Get_processor_name(name, namelen);
  //MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  myRank = MPI::COMM_WORLD.Get_rank ( );

  if(num_procs > MAX_PROCS){
    cout<< "too many processes: "<<num_procs;
    exit(1);
  }
  printf("Process %d out of %d realp %s \n", myRank, num_procs,name);

  srand48(myRank);  
  initMC(argc, argv);
  

  if(myRank==MASTER){
    initBetas(minBeta,maxBeta);
  }

  MPI::COMM_WORLD.Bcast(&node2betaID,num_procs,MPI::INT, MASTER);
  MPI::COMM_WORLD.Bcast(&betas,num_procs,MPI::DOUBLE, MASTER);
  MPI::COMM_WORLD.Bcast(&betaID2node,num_procs,MPI::INT, MASTER);
  // MPI::COMM_WORLD.Barrier();
  myBeta=betas[node2betaID[myRank]];

  
  for(int swaps=0;swaps<total_swaps;swaps++){
    //cout<< "MC process: "<<myRank<< " at beta: "<< myBeta<<" ";
    //cout<< "betaID "<< node2betaID[myRank]<<endl;  
    int myEnergy = MC(steps, myBeta,node2betaID[myRank]);
    MPI::COMM_WORLD.Gather(&myEnergy,1,MPI::INT,
			   &energies,1, MPI::INT,
			   MASTER) ; 
    if(myRank==MASTER){
      //cout <<endl;
      int start =0;// swap even
      if (drand48()<0.5) start=1; //swap uneven
      for(int i=start;i<num_procs-1;i=i+2){
	trySwap(i,i+1);
      }
    }
    MPI::COMM_WORLD.Bcast(&node2betaID,num_procs,MPI::INT, MASTER);
    MPI::COMM_WORLD.Bcast(&betaID2node,num_procs,MPI::INT, MASTER);
    myBeta=betas[node2betaID[myRank]];
  }
  double myEnergy = MC(steps,myBeta,node2betaID[myRank]);

  getMCStats(num_procs,myRank);
  cout << "close down process "<<myRank<<endl;
  // MPI::Finalize();
  exit(0);
}


bool trySwap(int betaID1, int betaID2){
  //cout <<"trial: "<<betaID1<<" "<<betaID2<<endl;
  int node1 = betaID2node[betaID1];
  int node2 = betaID2node[betaID2];
  double beta1 = betas[betaID1];
  double beta2 = betas[betaID2];
  double energy1 =energies[node1];
  double energy2 =energies[node2];
  // cout<< "energies: " <<energy1<<" "<<energy2<<endl;
  double dE = (double) (energy1-energy2);
  double dB = beta1-beta2;
  double pAccSwap = exp(dE*dB);
  if(drand48()<pAccSwap){
    //cout << "SWAPPING " << beta1 <<" with energy: "<< energy1 <<" at node "<<node1;
    //cout << " with " << beta2<<" with energy: "<< energy2<< " at node " <<node2<<endl;
    betaID2node[betaID1] =node2;
    betaID2node[betaID2] =node1;
    node2betaID[node1]=betaID2;
    node2betaID[node2]=betaID1;
    //transitionRates->changeBeta(Cnat,ligC);
    return true;
  }else{
    return false;
  }
}




int initBetas(double minBeta,double maxBeta){
  if(num_procs ==1){
    betas[0]= (minBeta + maxBeta)/2;
     betaID2node[0]=0;
     node2betaID[0]=0;
  }else{
    double step=(double)(maxBeta - minBeta)/(double) (num_procs-1);
    for (int i=0;i<num_procs;i++){
      betas[i]= minBeta + i*step;
      betaID2node[i]=i;
      node2betaID[i]=i;
    }
  }
}
