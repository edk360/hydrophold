
#include "WaterStats.hpp"
#include "Lattice.hpp"
#include "Chain.hpp"
#include "AA.hpp"
#include "Stats.hpp"
#include "StatsMethods.hpp"

#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <sstream>

/////////////////////////////////////////////////////////////////////
//// FUNCTIONS FOR WATERCONTACTS
#define MAX_CONTACTS 7
#define BURIED 0
#define SURFACE 1
#define HYDRATED 2

#define SURFACECUTOFF 4

double m[MAX_CONTACTS]={0,SURWEIGHT,SURWEIGHT,SURWEIGHT,1,1,1};

double T0[MAX_CONTACTS]={0, SURFACET0, SURFACET0, SURFACET0, SOLVATET0, SOLVATET0, SOLVATET0};
double alpha[MAX_CONTACTS]={0, SURALPHA, SURALPHA, SURALPHA, SOLALPHA, SOLALPHA, SOLALPHA};


int status[MAX_CONTACTS]={BURIED,SURFACE,SURFACE,SURFACE,HYDRATED,HYDRATED,HYDRATED};

void WaterStats::initGlobalWaterStats(Lattice *ll){\
  l=ll;
  stats.wcN=0;
  stats.wcHN=0;
  stats.solvN=0;
  stats.solvHN=0;
  stats.solvE=0;
  stats.surfaceHN=0;
  stats.solvatedHN=0;
  
  totalE=0;
}

void WaterStats::setGlobalWaterStats(){
  stats = calculateGlobalWaterStats();
  Stats s;
  s.getLatticeStats(l);
  totalE = stats.solvE + s.getEtot();
}

void WaterStats::addChain2Global(Chain *c, Stats & chainStats){
  Wstats chainstats= getWaterStats(c);
  stats.wcN += chainstats.wcN;
  stats.wcHN += chainstats.wcHN;
  stats.solvN += chainstats.solvN;
  stats.solvHN += chainstats.solvHN;
  stats.solvE += chainstats.solvE;
  stats.surfaceHN += chainstats.surfaceHN;
  stats.solvatedHN += chainstats.solvatedHN;
  
  totalE +=chainstats.solvE + chainStats.getEtot();
}

void WaterStats::subtractChainGlobal(Chain *c, Stats & chainStats){
  Wstats chainstats= getWaterStats(c);
  stats.wcN -= chainstats.wcN;
  stats.wcHN -= chainstats.wcHN;
  stats.solvN -= chainstats.solvN;
  stats.solvHN -= chainstats.solvHN;
  stats.solvE -= chainstats.solvE;
  stats.surfaceHN -= chainstats.surfaceHN;
  stats.solvatedHN -= chainstats.solvatedHN;
  totalE -= (chainstats.solvE +chainStats.getEtot());
}

Wstats WaterStats::getWaterStats(Chain *c){
  Wstats out;
  int mult;
  double tmult=1;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0;
  out.solvE=0;
  out.surfaceHN=0;
  out.solvatedHN=0;
  for(int n=0;n<c->N;n++){
    mult=1;
    Residue * res= c->residues[n];
    int reswc=0;
    for(int k=0;k<6;k++){
      Pos posNB =local[k]+ res->pos;
      posNB.periodicBoundary();
      Residue * resNB = l->getResidue(posNB);
      if(resNB==NULL){
	out.wcN++;
	reswc++;
        if(l->aaInt->getInteraction(res->aa,AA::WATER)> 0){
	  out.wcHN++;
	}
      }
    }
    if(reswc>0){
      out.solvN++;
      if(AA::WATER >= 0){
	if(App::NewPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){
	  mult=reswc;
	}
	else if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){
	  tmult=m[reswc];
	}

	if(App::TempDepPotential){
		if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){
		  if(App::SeparateT0){
			 out.solvE += lround(tmult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),alpha[reswc], T0[reswc]));
		  }
		  else{
		    out.solvE += lround(tmult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),App::alpha, App::T0));
		  }
		}
		else{
          out.solvE += mult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),App::alpha, App::T0);
		}
	}
    else{
		if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER) > 0){
		  out.solvE += lround( tmult * l->aaInt->getInteraction(res->aa, AA::WATER));
		}
		else{
	      out.solvE += mult * l->aaInt->getInteraction(res->aa, AA::WATER);
		}
    }
	if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
	  out.solvHN++;
	  if(reswc>0){
	    if(reswc<SURFACECUTOFF){
	      out.surfaceHN++;
	    }
	    else{
	      out.solvatedHN++;
	    }
	  }
	}
  }

    } 
  }
  return out;
}

Wstats WaterStats::calculateGlobalWaterStats(){
  Wstats out;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0;
  out.solvE=0;
  out.solvatedHN=0;
  out.surfaceHN=0;
  for(int nc=0;nc<l->nChains;nc++){
    Chain * ch = l->chains[nc];
    Wstats chainstats = getWaterStats(ch);
    out.wcN += chainstats.wcN;
    out.wcHN += chainstats.wcHN;
    out.solvN +=chainstats.solvN;
    out.solvHN += chainstats.solvHN;
    out.solvE +=chainstats.solvE;
    out.surfaceHN += chainstats.surfaceHN++;
    out.solvatedHN += chainstats.solvatedHN++;
  }
  return out;
}

Wstats WaterStats::sumGlobalWaterStats(){
  Wstats out;
  int mult=1;
  double tmult=1;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0;
  out.solvE=0;
  out.solvatedHN=0;
  out.surfaceHN=0;
  for(int nc=0;nc<l->nChains;nc++){
    Chain * ch = l->chains[nc];
    for(int n=0;n<ch->N;n++){
      mult=1;
      Residue * res= ch->residues[n];
      out.wcN+= res->waterContacts;
      if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
        out.wcHN += res->waterContacts;
      }
      if(res->waterContacts){
        out.solvN++;
        if(AA::WATER >= 0){
          if(App::NewPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){

	        mult=res->waterContacts;
	      }
          else if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){
            tmult=m[res->waterContacts];
   	      }
	      if(App::TempDepPotential){
	    	if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER)>0){

	            if( App::SeparateT0){
	            	out.solvE += lround(tmult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),alpha[res->waterContacts],T0[res->waterContacts]));
	            }
	            else{
	            	 out.solvE += lround(tmult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),App::alpha, App::T0));

	            }
	    	}
	    	else{
	          out.solvE += mult * l->aaInt->getTempInteraction(res->aa, AA::WATER,l->getRealBeta(),App::alpha, App::T0);
	    	}
	      }
	      else{
		    if(App::HalfPotential&& l->aaInt->getInteraction(res->aa,AA::WATER)>0){
		      out.solvE += lround(tmult * l->aaInt->getInteraction(res->aa,AA::WATER));
		    }
		    else{
		      out.solvE += mult * l->aaInt->getInteraction(res->aa,AA::WATER);
		    }
          }
          if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
            out.solvHN++;
            if(res->waterContacts>0){
            	if(res->waterContacts<SURFACECUTOFF){
            		out.surfaceHN++;
            	}
            	else{
            		out.solvatedHN++;
            	}
            }
          }
        }
      }
    } 
  }
  return out;
}

void WaterStats::printWstats(){
  cout<<"wcN "<<stats.wcN << " wcHN " << stats.wcHN <<" solvN "<<stats.solvN<< " solvHN " <<stats.solvHN<< " solvE "<< stats.solvE << " solvatedHN " << stats.solvatedHN << " surfaceHN " << stats.surfaceHN<<endl;
  cout<<"totalE "<<totalE<<endl;
}

/// should be static
void WaterStats::printWstats(Wstats & in){
  cout<<"wcN "<<in.wcN <<" wcHN " << in.wcHN <<" solvN "<<in.solvN<< " solvHN " << in.solvHN << " solvE "<<in.solvE<<endl;
}

string WaterStats::print2string(){
  std::stringstream myStream;
  myStream <<"wcN "<<stats.wcN<< " wcHN " << stats.wcHN << " solvN "<<stats.solvN<< " solvHN " <<stats.solvHN<< " solvE "<<stats.solvE<<endl;
  return myStream.str(); 
}

bool WaterStats::checkWaterStats(){
  Wstats calc=calculateGlobalWaterStats();
  Wstats sum = sumGlobalWaterStats();
  int totEcalc = calc.solvE + l->stats.getEtot();
  if(stats.wcN !=calc.wcN || calc.wcN != sum.wcN ||
      stats.wcHN != calc.wcHN || calc.wcHN != sum.wcHN ||
     stats.solvN !=calc.solvN || calc.solvN != sum.solvN ||
     stats.solvE !=calc.solvE || calc.solvE != sum.solvE ||
     totalE != totEcalc  ||
     calc.solvHN !=sum.solvHN ||
     calc.solvatedHN != sum.solvatedHN ||
     calc.surfaceHN != sum.surfaceHN
     ){
    cout <<"Error in water contacts"<<endl;
    cout << "calculated: "<<endl;
    printWstats(calc);
    cout<<"totEcalc "<<totEcalc<<endl;
    cout << "additive: "<<endl;
    printWstats();
    //cout<<"totalE "<<totalE<<endl;
    cout << "summed: " <<endl;
    printWstats(sum);

    Stats newStats;
    newStats.getLatticeStats(l);
    cout<< "calculated stats:"<<endl;
    newStats.printCout();
    cout<< "additive stats:"<<endl;
    l->stats.printCout();
    l->writePDBMultiChain("errorInWCs.pdb");
    exit(1);
    return false;
  }else{
    //cout << "calculated: "<<calc<<endl;
    //cout << "additive: "<< waterStats<<endl;
    //cout << "summed: " << sum<<endl;
    //stats.printCout();   
    return true;
  }
}




/// Initialises the residues
void WaterStats::initChainWC(Chain *c){
  for(int n=0;n<c->N;n++){
    Residue * res= c->residues[n];
    res->waterContacts=0;
    res->localNewWC=0;
    res->localOldWC=0;
    res->visitedWC=false;
    res->newWC=0;
    for(int k=0;k<6;k++){
      Pos posNB =local[k]+ res->pos;
      posNB.periodicBoundary();
      Residue * resNB = l->getResidue(posNB);
      if(resNB==NULL){  
	res->waterContacts++;
      }
    }
  }
}

//// FUNCTIONS FOR AA SWAP
Wstats WaterStats::dEAASwap(const Pos & pos, int oldaa, int newaa){
  Wstats out;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0;
  out.solvE=0;
  int reswc=0;
  int multnew=1;
  int multold=1;
  if(AA::WATER >= 0){    
    for(int k=0;k<6;k++){
      Pos posNB =local[k]+ pos;
      posNB.periodicBoundary();
      Residue * resNB = l->getResidue(posNB);
      if(resNB==NULL){
	reswc++;
      }
    }
    if(reswc>0){
      if(App::NewPotential && l->aaInt->getInteraction(newaa,AA::WATER) > 0){
	multnew=reswc;
      }
      else if(App::HalfPotential && l->aaInt->getInteraction(newaa,AA::WATER) > 0){
	multnew=m[reswc];
      }
      if(App::NewPotential && l->aaInt->getInteraction(oldaa,AA::WATER)>0){
	multold=reswc;
      }
      else if(App::HalfPotential && l->aaInt->getInteraction(oldaa,AA::WATER)>0){
	multold=m[reswc];
      }
      if(App::TempDepPotential){
	out.solvE += multnew * l->aaInt->getTempInteraction(newaa, AA::WATER, l->getRealBeta(),App::alpha, App::T0) - multold * l->aaInt->getTempInteraction(oldaa,AA::WATER,l->getRealBeta(),App::alpha, App::T0);
      }
      else{
        out.solvE +=  (int)multnew*l->aaInt->getInteraction(newaa, AA::WATER) -
		     (int)multold*l->aaInt->getInteraction(oldaa, AA::WATER);
      }
    }
  } 
  return out;
}

Wstats WaterStats::dEAAUnfoldSwap(const Pos & pos, int oldaa, int newaa){
  Wstats out;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0; //update
  out.solvE=0;
  int multnew=1;
  int multold=1;
  cout <<" HHH" << endl;
  if(App::NewPotential && l-> aaInt->getInteraction(newaa, AA::WATER) > 0){
    multnew=4;
  }
  else if(App::HalfPotential && l-> aaInt->getInteraction(newaa, AA::WATER) > 0){
    multnew=m[4];
  }
  if(App::NewPotential && l-> aaInt->getInteraction(oldaa,AA::WATER)>0){
    multold=4;
  }
  else if(App::HalfPotential && l-> aaInt->getInteraction(oldaa,AA::WATER)>0){
    multold=m[4];
  }
  if(App::TempDepPotential){
    out.solvE = (int) (multnew * l->aaInt->getTempInteraction(newaa, AA::WATER,l->getRealBeta(),App::alpha, App::T0)) - (int)(multold * l->aaInt->getTempInteraction(oldaa, AA::WATER,l->getRealBeta(),App::alpha, App::T0));
  }
  else{
    out.solvE = (int) (multnew * l->aaInt->getInteraction(newaa, AA::WATER)) - (int)(multold * l->aaInt->getInteraction(oldaa, AA::WATER));
  }
  return out;
}





/////////////////////////////////////////////////////////
//// FUNCTIONS FOR MOVES

void WaterStats::localWC(const Pos & pos, Residue * res, bool old){
  //// empty position
  if(res==NULL){ 
    for(int k=0;k<6;k++){
      Pos posNB =local[k]+ pos;
      posNB.periodicBoundary();
      Residue * resNB = l->getResidue(posNB);
      if(resNB!=NULL){  
	affectedResidues.push_back(resNB);
	if(old){
	  resNB->localOldWC++;
	}else{
	  resNB->localNewWC++;
	}   
      }
    } 
  //// residue position
  }else{
    for(int k=0;k<6;k++){
      Pos posNB =local[k]+ pos;
      posNB.periodicBoundary();
      Residue * resNB = l->getResidue(posNB);
      if(resNB == NULL){  
	affectedResidues.push_back(res);
	if(old){
	  res->localOldWC++;
	}else{
	  res->localNewWC++;
	}   
      }
    }
  }
} 




Wstats WaterStats::calculateChangeInWC(){
  Wstats out;
  out.wcN=0;
  out.wcHN=0;
  out.solvN=0;
  out.solvHN=0;
  out.solvE=0;
  out.solvatedHN=0;
  out.surfaceHN=0;
  for(int i=0;i<(int)affectedResidues.size();i++){
    Residue * res = affectedResidues[i];
    if(res==NULL){
      cout<<i<<" res does not exist"<<endl;
      exit(1);
    }
    //// should only visit each residue once
    if(!res->visitedWC){
      int diff = res->localNewWC - res->localOldWC;
      int interactiondiff=0;
      if(App::HalfPotential){
    	  interactiondiff=m[res->waterContacts+diff]-m[res->waterContacts];
      }
     // cout << "resnewwc: " <<  res->waterContacts + diff <<" resoldWC: " << res->waterContacts<< " mnewWC: " << m[res->waterContacts+diff] <<" moldWC: " << m[res->waterContacts] << endl;
      out.wcN += diff;
      if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
	    out.wcHN+=diff;
      }
      res->newWC = res->waterContacts + diff;

      if(App::NewPotential && l->aaInt->getInteraction(res->aa,AA::WATER) > 0){
	    if(App::TempDepPotential){
	      out.solvE += diff * l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),App::alpha, App::T0);
	    }
	    else{
	      out.solvE += diff * l->aaInt->getInteraction(res->aa,AA::WATER);
        }
      }
      else if(App::HalfPotential && l->aaInt->getInteraction(res->aa,AA::WATER) > 0){
	    if(App::TempDepPotential){
          if(App::SeparateT0){
  	    	out.solvE += lround(m[res->waterContacts+diff] * l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),alpha[res->waterContacts+diff], T0[res->waterContacts+diff]))-
  	    		  lround(m[res->waterContacts]* l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),alpha[res->waterContacts], T0[res->waterContacts]));
          }
          else{
	    	out.solvE += lround (m[res->waterContacts+diff] * l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),App::alpha, App::T0))-
	    	    lround(m[res->waterContacts]* l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),App::alpha, App::T0));
          }
	    }
	    else{
	    	out.solvE +=  lround(m[res->waterContacts+diff]* l->aaInt->getInteraction(res->aa,AA::WATER)) - lround(m[res->waterContacts]*  l->aaInt->getInteraction(res->aa,AA::WATER));
        }
	  }
      if(res->waterContacts + diff== 0){
	    out.solvN--;
	    if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
	      out.solvHN--;
          if(status[res->waterContacts] == SURFACE){
        	  out.surfaceHN--;
          }
          if(status[res->waterContacts] == HYDRATED){
        	  out.solvatedHN--;
          }
	    }
	    if(AA::WATER >= 0){
          if( ((!App::HalfPotential && !App::NewPotential) || l->aaInt->getInteraction(res->aa,AA::WATER)<=0)){
	        if(App::TempDepPotential){
              out.solvE -= l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),App::alpha, App::T0);
	        }
	        else{
	          out.solvE -= l->aaInt->getInteraction(res->aa, AA::WATER);
	        }
  	      }
	    }
      }
      if(res->waterContacts ==0 && res->localNewWC !=0){
	    out.solvN++;
	    if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
          out.solvHN++;
          if(status[res->waterContacts + diff] == SURFACE){
        	  out.surfaceHN++;
          }
          if(status[res->waterContacts + diff] == HYDRATED){
        	  out.solvatedHN++;
          }
	    }
	    if(AA::WATER >= 0){
	      if( ((!App::HalfPotential && !App::NewPotential) || l->aaInt->getInteraction(res->aa,AA::WATER)<=0)){
	        if(App::TempDepPotential){
	          out.solvE += l->aaInt->getTempInteraction(res->aa,AA::WATER,l->getRealBeta(),App::alpha, App::T0);
	        }
	        else{
	          out.solvE +=  l->aaInt->getInteraction(res->aa, AA::WATER);
	        }
          }
	    }
      }
      if(status[res->waterContacts]==HYDRATED && status[res->waterContacts + diff] == SURFACE){
    	  if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
        	out.solvatedHN--;
        	out.surfaceHN++;
    	  }
      }
      if(status[res->waterContacts+ diff]==HYDRATED && status[res->waterContacts] == SURFACE){
    	  if(l->aaInt->getInteraction(res->aa,AA::WATER)>0){
        	out.solvatedHN++;
        	out.surfaceHN--;
    	  }
      }
      res->visitedWC=true;
    }
  }
  return out;
}

void WaterStats::updateWC(Wstats change,int changeEtotal){
  for(int i=0;i<(int)affectedResidues.size();i++){
    Residue * res = affectedResidues[i];
    res->waterContacts = res->newWC;
  }
  stats.wcN+=change.wcN;
  stats.wcHN+=change.wcHN;
  stats.solvN+=change.solvN;
  stats.solvHN+=change.solvHN;
  stats.solvE+=change.solvE;
  stats.solvatedHN += change.solvatedHN;
  stats.surfaceHN += change.surfaceHN;
  totalE += changeEtotal;
}


void WaterStats::cleanUpWC(){
  //// clean all local WC variables in residues
  for(int i=0;i<(int)affectedResidues.size();i++){
    Residue * res = affectedResidues[i];
    res->localNewWC=0;
    res->localOldWC=0;
    res->visitedWC=false;
    res->newWC=0;
    // affectedResidues[i]=NULL;
  }
  //// empty affectedResidues vector
  affectedResidues.resize(0);
}
