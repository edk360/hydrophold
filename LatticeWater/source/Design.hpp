#ifndef _DESIGN_H_
#define _DESIGN_H_

#include <string.h>
#include <stdio.h>
#include <iomanip>
using namespace std;

class Lattice;
class AADistr;

class Design{
public:
  Design();
  void designProcedure(double beta, long numSteps);
  void writeDesignStats(string s);
  void init(string fn_in);
  void initOldFormat(string fn_in);
private:
  void changeAA(int chain);
  void initCountAA();
  double getSystemVar();
  void checkVariance();
  Lattice * mainLattice;
  AADistr * aaDistr;
};

#endif
